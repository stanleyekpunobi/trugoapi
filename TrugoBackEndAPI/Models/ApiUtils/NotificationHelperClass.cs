﻿using MimeKit;
using RazorEngine;
using RazorEngine.Templating;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using TrugoBackEndAPI.Models.DalClasses;
using TrugoBackEndAPI.Models.DatabaseModelss;
using TruGoClassLibrary.Classes;

namespace TrugoBackEndAPI.Models.ApiUtils
{
    public class NotificationHelperClass
    {
        //email templates folder location
        private static readonly string TemplateFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Views/Shared");

        private string MailGunPort { get; } = ConfigurationManager.AppSettings.Get("MailGunPort");

        private string MailGunClient { get; } = ConfigurationManager.AppSettings.Get("MailGunClient");

        private string MailGunUsername { get; } = ConfigurationManager.AppSettings.Get("MailGunUsername");

        private string MailGunPassword { get; } = ConfigurationManager.AppSettings.Get("MailGunPassword");

        //send confirmation email
        public bool SendConfirmationEmail(ConfirmEmailClass model, string code)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;
            try
            {
                var emailBody = GetConfirmEmailBody(model, code);
                var email = new MimeMessage();


                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";

                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(model.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }

            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //send invoice invitation email
        public async Task<bool> SendInvoiceInvitationEmail(ConfirmEmailClass model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;
            try
            {
                var emailBody = GetInvitationEmailBody(model);
                var email = new MimeMessage();

                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";

                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(model.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    await client.SendAsync(email);
                    client.Disconnect(true);
                    response = true;
                }
            }

            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //send invoice approval email
        public async Task<bool> SendInvoiceModerationEmailAsync(string userid, int id, bool isApproved)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;

            try
            {
                var user = new ApplicationUser();
                using (var dbContext = new ApplicationDbContext())
                {
                    user = dbContext.Users.Find(userid);
                }

                var emailBody = GetInvoiceApprovalStatusBody(userid, id, isApproved);

                var email = new MimeMessage();

                email.Body = new TextPart("html", emailBody);

                email.Subject = "TruGo";

                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));

                email.To.Add(new MailboxAddress(user.Email));

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    await client.SendAsync(email);
                    client.Disconnect(true);
                    response = true;
                }
            }

            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        public bool SendSuccessfulInvoicePurchaseEmail(InvoicePurchaseModel model, ApplicationUser user)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;

            try
            {
                var emailBody = GetInvoicePurchaseEmailBody(model);
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(user.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //send withdrawal approval email
        public bool SendWithdrawalApprovalEmail(AccountSetupModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;

            try
            {

                var emailBody = GetWithdrawalApprovalEmailBody(model);
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(model.ApplicationUser.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        public bool SendCreditDeclineEmail(string userid)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;

            var user = new ApplicationUser();

            try
            {

                using (var dbContext = new ApplicationDbContext())
                {
                    user = dbContext.Users.Find(userid);
                }

                var emailBody = GetCreditRequestDeclineEmailBody();
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(user.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        public bool SendWithdrawalDeclineEmail(string userid)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;

            var user = new ApplicationUser();

            try
            {

                using (var dbContext = new ApplicationDbContext())
                {
                    user = dbContext.Users.Find(userid);
                }

                var emailBody = GetWithdrawalDeclineEmailBody();
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(user.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }



        //send credit approval email
        public bool SendCreditApprovalEmail(AccountSetupModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;

            try
            {

                var emailBody = GetCreditApprovalEmailBody(model);
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(model.ApplicationUser.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //send profile approval email
        public bool SendApprovalEmail(ApplicationUser user)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;
            try
            {
                var profile = new AccountSetupModel();
                using (var dbContext = new ApplicationDbContext())
                {
                    profile = dbContext.Profiles.Find(user.Id);
                }
                var emailBody = GetApprovalEmailBody(profile);
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(user.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //send profile decline email
        public bool SendDeclineEmail(ApplicationUser user, string message)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;
            try
            {
                var profile = new AccountSetupModel();
                using (var dbContext = new ApplicationDbContext())
                {
                    profile = dbContext.Profiles.Find(user.Id);
                }
                profile.ReferralName = message;
                var emailBody = GetDeclineEmailBody(profile);
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(user.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }

            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //send password reset email
        public bool SendPasswordResetEmail(PasswordResetClass model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var response = false;
            try
            {
                var emailBody = GetResetEmailBody(model);
                var email = new MimeMessage();
                email.Body = new TextPart("html", emailBody);
                email.Subject = "TruGo";
                email.From.Add(new MailboxAddress("TruGo", "noreply@notification.trugo.co"));
                email.To.Add(new MailboxAddress(model.Email));
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(MailGunClient, Convert.ToInt32(MailGunPort), false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(MailGunUsername, MailGunPassword);
                    client.Send(email);
                    client.Disconnect(true);
                    response = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //get profile approval email body
        private string GetApprovalEmailBody(AccountSetupModel user)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "accountapprovedemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "welcomekey", typeof(ApplicationUser), user, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get profile decline email body
        private string GetDeclineEmailBody(AccountSetupModel user)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "accountdeclineemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "declinekey", typeof(ApplicationUser), user, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get confirm email template body
        private string GetConfirmEmailBody(ConfirmEmailClass model, string code)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "activateaccountemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "welcomekey", typeof(ConfirmEmailClass), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get invoice invitation email body
        private string GetInvitationEmailBody(ConfirmEmailClass model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "invoiceinvitationemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "invoiceinvitationkey", typeof(ConfirmEmailClass), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get reset email body
        private string GetResetEmailBody(PasswordResetClass model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "resetaccountpassword.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "resetkey", typeof(ConfirmEmailClass), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get credit approval email body
        private string GetCreditApprovalEmailBody(AccountSetupModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "creditapprovedemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "approvecreditkey", typeof(AccountSetupModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get withdrawal approval email body
        private string GetWithdrawalApprovalEmailBody(AccountSetupModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "withdrawalapprovedemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "approvewithdrawalkey", typeof(AccountSetupModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get invoice purchase email body
        private string GetInvoicePurchaseEmailBody(InvoicePurchaseModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "invoicepurchaseemailbody.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "invoicepurchasesuccesskey", typeof(InvoicePurchaseModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        private string GetWithdrawalDeclineEmailBody()
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "withdrawaldeclineemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "declinewithdrawalkey", null, null, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        public string GetCreditRequestDeclineEmailBody()
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "withdrawaldeclineemailtemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "declinecreditrequestkey", null, null, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        private string GetInvoiceApprovalStatusBody(string userid, int id, bool isApproval)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var result = string.Empty;

            var invoice = new InvoiceModel();

            using (var dbContext = new ApplicationDbContext())
            {
                invoice = dbContext.Invoices.Find(id);
            }

            try
            {
                if (isApproval)
                {
                    invoice.AdminStatus = "approved";
                }
                else
                {

                    invoice.AdminStatus = "not approved";
                }

                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "invoiceapprovaltemplate.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "invoiceapprovalkey", typeof(InvoiceModel), invoice, null);

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        public void AddNotification(string userid, string message, DateTime dateCreated, bool isViewed)
        {
            using (var dbContext = new ApplicationDbContext())
            {
                dbContext.Notifications.Add(new NotificationModel(userid, message, dateCreated, isViewed));
                dbContext.SaveChanges();
            }
        }
    }
}
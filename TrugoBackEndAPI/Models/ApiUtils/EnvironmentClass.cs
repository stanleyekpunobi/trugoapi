﻿using System.Configuration;

namespace TrugoBackEndAPI.Models.ApiUtils
{
    public class EnvironmentClass
    {
        //set environment to dev or live
        private static bool IsAppTest { get; set; } = false;

        //database connection string to use
        public static string DatabaseConnection
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[$"{ GetConnectionString(IsAppTest)}"].ConnectionString;
            }
        }

        //api base url to use
        public static string ApiBaseUrl {
            get
            {
                return ConfigurationManager.AppSettings.Get(GetApiBaseUrl(IsAppTest));
            }
        }

        //return connection string based on environment
        private static string GetConnectionString(bool isAppTest)
        {
            if (isAppTest == true)
            {
                return "Devdb";
            }
            else
            {
                return "Livedb";
            }
        }

        //return api url based on environment
        private static string GetApiBaseUrl(bool isAppTest)
        {
            if (isAppTest == true)
            {
                return "DevUrl";
            }
            else
            {
                return "LiveUrl";
            }
        }
    }
}
﻿using System.Linq;
using System.Net.Http;

namespace TrugoBackEndAPI.Models.ApiUtils
{
    public class ApiHelperClass:NotificationHelperClass
    {
        public string GetReturnEmailHeaderValue(HttpRequestMessage request)
        {
            var headers = request.Headers;
            if (headers.Contains("emailurl"))
            {
                var callback = headers.GetValues("emailurl").First();
                return callback;
            }
            else
            {
                return null;
            }
        }
    }
}
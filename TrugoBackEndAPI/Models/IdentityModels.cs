﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using TrackerEnabledDbContext.Identity;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DatabaseModelss;

namespace TrugoBackEndAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public bool AccountApproved { get; set; }
        public bool AccountVerified { get; set; }
        public virtual AccountSetupModel Profiles { get; set; }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string roleName) : base(roleName) { }

    }

    public class ApplicationDbContext : TrackerIdentityContext<ApplicationUser>
    {

        public ApplicationDbContext()
            : base($"{EnvironmentClass.DatabaseConnection}")
        {
        }

        public virtual DbSet<AccountSetupModel> Profiles { get; set; }
        public virtual DbSet<IncomeStatementModel> IncomeStatement { get; set; }
        public virtual DbSet<CreditRequestModel> CreditRequest { get; set; }
        public virtual DbSet<WithdrawCreditModel> WithdrawCredit { get; set; }
        public virtual DbSet<InvoiceModel> Invoices { get; set; }
        public virtual DbSet<InvoicePurchaseModel> Payments { get; set; }

        public virtual DbSet<NotificationModel> Notifications { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TrugoBackEndAPI.Models.DatabaseModelss;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Models.DalClasses
{
    public class InvestorDal : UserAccountDal
    {
        public DashboardChartDataResponseModel GetDashboardChartData(string userid)
        {
            var dashboardChartData = new DashboardChartDataResponseModel();

            var thismonthData = GetMonthlyChartData(userid);

            dashboardChartData.ThisMonthChartData = thismonthData;
          
            var chartanalysis = GetChartDataAnalysis(userid);

            dashboardChartData.MonthlyChartData = chartanalysis;

            dashboardChartData.MonthlyLabels = GetMonthlyLabels(dashboardChartData.ThisMonthChartData);

            return dashboardChartData;
        }

        public async Task<bool> PurchaseInvoice(InvoicePurchaseModel model)
        {
            bool isSucessful = false;
            var user = new ApplicationUser();
            var purchases = new List<InvoicePurchaseModel>();
            using (var dbContext = new ApplicationDbContext())
            {
                user = dbContext.Users.Find(model.ProfileId);
                var invoice = dbContext.Invoices.SingleOrDefault(m => m.Id == model.InvoiceId && m.InvoiceGuid == model.Code);

                if (invoice != null)
                {
                    if (invoice.AdminStatus == "approved" && invoice.UserStatus == "on sale")
                    {
                        purchases = dbContext.Payments.Where(m => m.InvoiceId == model.InvoiceId && m.Code == model.Code).ToList();

                       // var purchaseSum = purchases.Sum(m => m.Amount);

                        if (purchases.Sum(m => m.Amount) >= invoice.SalePrice)
                        {

                        }
                        else
                        {
                            model.Date_Created = DateTime.Now;

                            dbContext.Payments.Add(model);

                            await dbContext.SaveChangesAsync();

                            isSucessful = true;

                        }

                    }
                }
            }
            if (isSucessful)
            {
                SendSuccessfulInvoicePurchaseEmail(model, user);
            }
            else
            {

            }

            return isSucessful;
        }

        private List<ChartData> GetChartDataAnalysis(string userid)
        {
            var chartData = new List<ChartData>();

            var chartDataResult = new List<ChartData>();

            var quaterlyData = new List<InvoicePurchaseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                quaterlyData = dbContext.Payments.Where(m => m.ProfileId == userid).ToList();
            }
            var groupedquaterlyCount = quaterlyData.GroupBy(m => m.Date_Created.Month, (key, value) => new { month = key, total = value.Count() }).OrderBy(z => z.month).ToList(); 

            var gropuedquaterlySum = quaterlyData.GroupBy(m => m.Date_Created.Month, (key, value) => new { month = key, total = value.Sum(m => m.Amount) }).OrderBy(z => z.month).ToList(); 

            foreach (var item in groupedquaterlyCount)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "count",
                    DataValue = item.total,
                    Month = item.month,
                });
            }

            chartData = GetDataSummary(chartData);

            chartDataResult = AddChartDataToResultList(chartData, chartDataResult);

            chartData.Clear();

            foreach (var item in gropuedquaterlySum)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "sum",
                    DataValue = item.total,
                    Month = item.month,
                });
            }
            chartData = GetDataSummary(chartData);

            chartDataResult = AddChartDataToResultList(chartData, chartDataResult);
            
            return chartDataResult;
        }

        private List<ChartData> AddChartDataToResultList(List<ChartData> data, List<ChartData> result)
        {
            foreach (var item in data)
            {
                result.Add(item);
            }

            return result;
        }

        private List<ChartData> GetDataSummary(List<ChartData> modeldata)
        {
            string chartType = string.Empty;

            if (modeldata.Count() > 0)
            {
                chartType = modeldata.Where(m => m.Month != 0).FirstOrDefault().ChartType;
            }

            for (int i = 1; i <= 12; i++)
            {
                if (modeldata.Any(m => m.Month == i))
                {

                }
                else
                {
                    modeldata.Add(new ChartData
                    {
                        ChartType = chartType,
                        DataValue = 0,
                        DateValue = DateTime.Now,
                        Month = i,
                    });
                }
            }

            return modeldata;
        }

     
        private List<ChartData> GetMonthlyChartData(string userid)
        {
            var chartData = new List<ChartData>();

            var currentMonth = DateTime.Now.ToString("MM");

            var purchasesData = new List<InvoicePurchaseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                purchasesData = dbContext.Payments.Where(m => m.ProfileId == userid).ToList();
            }

            var groupedpurchasesCount = purchasesData.Where(m => m.Date_Created.ToString("MM") == currentMonth).GroupBy(m => m.Date_Created, (key, value) => new { month = key, total = value.Count() });

            var groupedpurchasesSum = purchasesData.Where(m => m.Date_Created.ToString("MM") == currentMonth).GroupBy(m => m.Date_Created, (key, value) => new { month = key, total = value.Sum(m => m.Amount) });

            foreach (var item in groupedpurchasesCount)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "investormonthlypurchasecount",
                    DataValue = item.total,
                    DateValue = item.month
                });
            }
            
            foreach (var item in groupedpurchasesSum)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "investormonthlypurchasesum",
                    DataValue = item.total,
                    DateValue = item.month
                });
            }

            return chartData;
        }

        private DateTime[] GetMonthlyLabels(List<ChartData> model)
        {
            var countdata = model.Where(m => m.ChartType == "investormonthlypurchasecount").ToList();

            var summodel = model.Where(m => m.ChartType == "investormonthlypurchasesum").ToList();

            summodel = FormateDate(summodel);

            countdata = FormateDate(countdata);

            var datelables = new List<DateTime>();

            var joinsumandcount = summodel.Join(countdata, s => s.DateValue, c => c.DateValue, (s,c) => new {date = s.DateValue }).ToList();

            foreach (var item in joinsumandcount)
            {
                datelables.Add(item.date);
            }

            return datelables.ToArray();
        }

        private List<ChartData> FormateDate(List<ChartData> model)
        {
            foreach (var item in model)
            {
                item.DateValue = DateTime.ParseExact(item.DateValue.ToString(), "MM/dd/YYYY", null);
            }

            return model;
        }

        private List<DateTime> GetDates(int year, int month)
        {
           var dates = Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                             .Select(day => new DateTime(year, month, day))
                             .ToList();

            return dates;
        }

        public InvestorDashboardResponseModel GetDashboardData(string userid)
        {
            var investorDashboard = new InvestorDashboardResponseModel();

            var topSellingInvoices = new List<InvoiceModel>();

            var invoicesInProgress = new List<InvoiceModel>();

            var processedInvoices = new List<InvoiceModel>();


            using (var dbContext = new ApplicationDbContext())
            {
                var userProfile = dbContext.Profiles.Find(userid);

                investorDashboard.CreditBalance = userProfile.TotalCredit;

                investorDashboard.TotalInvoices = dbContext.Payments.Where(m => m.ProfileId == userid).ToList().Distinct().Sum(m => m.Amount);

                investorDashboard.PendingInvoices = dbContext.Payments.Include("Invoice").Where(m => m.ProfileId == userid && m.Invoice.UserStatus == "onsale").ToList().Count();

                investorDashboard.TotalEarnings = dbContext.IncomeStatement.Where(m => m.ProfileId == userid && m.Type == "earnings").ToList().Sum(m => m.Amount);

                investorDashboard.InvoicesBought = dbContext.Payments.Where(m => m.ProfileId == userid).ToList().Distinct().Count();

                investorDashboard.PendingInvoices = dbContext.Payments.Include("Invoice").Where(m => m.ProfileId == userid && m.Invoice.AdminStatus == "processed").ToList().Count();

                var onProgress = dbContext.Payments.Include("Invoice").Where(m => m.ProfileId == userid && m.Invoice.UserStatus == "onsale").OrderByDescending(m => m.Date_Created).ToList().Take(5).Distinct();

                var processed = dbContext.Payments.Include("Invoice").Where(m => m.ProfileId == userid && m.Invoice.UserStatus == "sold").OrderByDescending(m => m.Date_Created).ToList().Take(5).Distinct();

                var topselling = dbContext.Payments.Include("Invoice").OrderByDescending(m => m.Date_Created).Where(m => m.Invoice.UserStatus == "onsale").ToList().Take(5).Distinct();


                if (onProgress != null)
                {
                    foreach (var item in onProgress)
                    {
                        invoicesInProgress.Add(dbContext.Invoices.Find(item.InvoiceId));
                    }
                }

                if (processed != null)
                {
                    foreach (var item in onProgress)
                    {
                        processedInvoices.Add(dbContext.Invoices.Find(item.InvoiceId));
                    }
                }

                if (topselling != null)
                {
                    foreach (var item in topselling)
                    {
                        topSellingInvoices.Add(dbContext.Invoices.Find(item.InvoiceId));
                    }
                }

            }

            foreach (var item in invoicesInProgress)
            {
                investorDashboard.Invoices.Add(new InvestorDashboardInvoiceModel
                {
                    Amount = item.Amount,
                    InvoiceStatus = "inprogress",
                    MaturityDate = item.DueDate,
                    SalePrice = item.SalePrice,
                    Title = item.InvoiceTitle,
                    Earning = item.Payments.Where(m => m.ProfileId == userid).Sum(m => m.Amount)
                });
            }

            foreach (var item in processedInvoices)
            {
                investorDashboard.Invoices.Add(new InvestorDashboardInvoiceModel
                {
                    Amount = item.Amount,
                    InvoiceStatus = "inprogress",
                    MaturityDate = item.DueDate,
                    SalePrice = item.SalePrice,
                    Title = item.InvoiceTitle,
                    Earning = item.Payments.Where(m => m.ProfileId == userid).Sum(m => m.Amount)
                });
            }

            foreach (var item in topSellingInvoices)
            {
                investorDashboard.Invoices.Add(new InvestorDashboardInvoiceModel
                {
                    Amount = item.Amount,
                    InvoiceStatus = "inprogress",
                    MaturityDate = item.DueDate,
                    SalePrice = item.SalePrice,
                    Title = item.InvoiceTitle,
                    Earning = item.Payments.Where(m => m.ProfileId == userid).Sum(m => m.Amount)
                });
            }

            return investorDashboard;
        }
    }
}
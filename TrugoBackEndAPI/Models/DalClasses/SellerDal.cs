﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrugoBackEndAPI.Models.DatabaseModelss;
using TruGoClassLibrary.Classes;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Models.DalClasses
{
    public class SellerDal : UserAccountDal
    {
        //generate invoice number
        private string GetInvoiceNumber(string count)
        {
            string invoicenumber = string.Format("INV-{0:00}", count);

            return invoicenumber;
        }

        //generate invoice number
        public string GenerateInvoiceNumber(string profileid)
        {
            int invoicecount = 0;

            string invoicenumber = string.Empty;

            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            try
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    invoicecount = dbContext.Invoices.Where(m => m.ProfileId == profileid).Count();
                }

                var invoicecountadded = invoicecount + 1;

                invoicenumber = GetInvoiceNumber(invoicecountadded.ToString("D7"));

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return invoicenumber;
        }

        public async Task<bool> CreateInvoice(InvoiceModel model)
        {
            bool isSuccessful = false;

            var profileExists = CheckProfile(model.ProfileId);

            if (!profileExists)
            {

            }
            else
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    dbContext.ConfigureUsername(model.ProfileId);

                    var invoice = dbContext.Invoices.Add(model);

                    await dbContext.SaveChangesAsync();

                    if (invoice != null)
                    {
                        isSuccessful = true;
                    }
                    else
                    {
                        isSuccessful = false;
                    }
                }

            }

            return isSuccessful;
        }

        public async Task<bool> Updateinvoice(InvoiceModel model)
        {
            bool isSuccessful = false;

            var profileExists = CheckProfile(model.ProfileId);

            if (!profileExists)
            {

            }
            else
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    dbContext.ConfigureUsername(model.ProfileId);

                    var invoice = dbContext.Invoices.Add(model);

                    await dbContext.SaveChangesAsync();

                    if (invoice != null)
                    {
                        isSuccessful = true;
                    }
                    else
                    {
                        isSuccessful = false;
                    }
                }

            }

            return isSuccessful;

        }

        public SellerDashboardResponseModel GetSellerDashboardData(string userid)
        {
            var dashboardData = new SellerDashboardResponseModel();

            var invoicesinProgess = new List<InvoiceModel>();

            var invoicesProcessed = new List<InvoiceModel>();

            var recentInvoices = new List<InvoiceModel>();

            var notifications = new List<NotificationModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                var user = dbContext.Profiles.Find(userid);
                dashboardData.CreditBalance = user.TotalCredit;
                dashboardData.InvoicesSold = dbContext.Invoices.Where(m => m.UserStatus == "sold" && m.ProfileId == userid).ToList().Count();
                dashboardData.NumberOfInvoicesSold = dbContext.Invoices.Where(m => m.UserStatus == "sold" && m.ProfileId == userid).ToList().Count();
                dashboardData.PendingInvoices = dbContext.Invoices.Where(m => m.AdminStatus == "notapproved" && m.ProfileId == userid).ToList().Count();
                dashboardData.TotalInvoicesSold = dbContext.Invoices.Where(m => m.UserStatus == "sold" && m.ProfileId == userid).ToList().Sum(m => m.Amount);
                dashboardData.InvoicesInProgress = dbContext.Invoices.Where(m => m.AdminStatus == "approved" && m.UserStatus == "onsale" && m.ProfileId == userid).ToList().Count();
                invoicesinProgess = dbContext.Invoices.Where(m => m.ProfileId == userid && (m.UserStatus != "sold" || m.UserStatus != "paused") && m.AdminStatus == "approved")
                    .OrderByDescending(m => m.Date_Created).Take(5).ToList();
                invoicesProcessed = dbContext.Invoices.Where(m => m.ProfileId == userid && (m.UserStatus == "sold" || m.UserStatus != "paused") && m.AdminStatus == "approved")
                    .OrderByDescending(m => m.Date_Created).Take(5).ToList();

                recentInvoices = dbContext.Invoices.Where(m => m.ProfileId == userid && (m.UserStatus == "onsale" && m.AdminStatus == "approved"))
                   .OrderByDescending(m => m.Date_Created).Take(5).ToList();

                notifications = dbContext.Notifications.Where(m => m.UserId == userid && m.IsViewed == false).Take(5).ToList();
            }

            foreach (var item in notifications)
            {
                dashboardData.Notifications.Add(new NotificationResponseModel
                {
                    Id = item.Id,
                    Date_Created = item.Date_Created,
                    IsViewed = item.IsViewed,
                    Message = item.Message,
                    UserId = item.UserId
                });
            }

            foreach (var item in invoicesProcessed)
            {
                dashboardData.Invoices.Add(new SellerDashboardInvoiceModel
                {
                    Amount = item.Amount,
                    InvoiceStatus = item.UserStatus,
                    InvoiceTitle = item.InvoiceTitle,
                    MaturityDate = item.DueDate,
                    SalePrice = item.SalePrice,
                    InvoiceType = "processed"
                });
            }

            foreach (var item in invoicesinProgess)
            {
                dashboardData.Invoices.Add(new SellerDashboardInvoiceModel
                {
                    Amount = item.Amount,
                    InvoiceStatus = item.UserStatus,
                    InvoiceTitle = item.InvoiceTitle,
                    MaturityDate = item.DueDate,
                    SalePrice = item.SalePrice,
                    InvoiceType = "inprogress"
                });
            }

            foreach (var item in recentInvoices)
            {
                dashboardData.Invoices.Add(new SellerDashboardInvoiceModel
                {
                    Amount = item.Amount,
                    InvoiceStatus = item.UserStatus,
                    InvoiceTitle = item.InvoiceTitle,
                    MaturityDate = item.DueDate,
                    SalePrice = item.SalePrice,
                    InvoiceType = "recent"
                });
            }

            return dashboardData;
        }

        public async Task<bool> SendInvoiceInvitations(string url, string invitations)
        {
            bool isSuccessful = false;
            var emails = invitations.Split(',');

            var confirmEmailClass = new ConfirmEmailClass();

            for (int i = 0; i < emails.Length; i++)
            {
                var email = emails[i].ToString();

                confirmEmailClass.Email = email;
                confirmEmailClass.CallbackUrl = url;

                isSuccessful = await SendInvoiceInvitationEmail(confirmEmailClass);
            }

            return isSuccessful;
        }

        public DashboardChartDataResponseModel GetDashboardChartData(string userid)
        {
            var dashboardChartData = new DashboardChartDataResponseModel();

            var thismonthData = GetMonthlyChartData(userid);


            dashboardChartData.ThisMonthChartData = FormatThisMonthChartData(thismonthData);

            var chartanalysis = GetChartDataAnalysis(userid);

            dashboardChartData.MonthlyChartData = chartanalysis;

            dashboardChartData.MonthlyLabels = GetMonthlyLabels(dashboardChartData.ThisMonthChartData);

            return dashboardChartData;
        }

        private List<ChartData> GetChartDataAnalysis(string userid)
        {
            var chartData = new List<ChartData>();

            var chartDataResult = new List<ChartData>();

            var invoiceData = new List<InvoiceModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                invoiceData = dbContext.Invoices.Where(m => m.ProfileId == userid).ToList();
            }

            var groupedTotalInvoiceSum = invoiceData.Where(s => s.AdminStatus == "approved" && s.Verified == true).GroupBy(m => m.Date_Created.Month, (key, value) => new { month = key, total = value.Count() }).OrderBy(z => z.month).ToList();

            var gropuedSoldInvoiceSum = invoiceData.Where(s => s.UserStatus == "sold").GroupBy(m => m.Date_Created.Month, (key, value) => new { month = key, total = value.Count() }).OrderBy(z => z.month).ToList();

            foreach (var item in groupedTotalInvoiceSum)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "numberofinvoices",
                    DataValue = item.total,
                    Month = item.month,
                });
            }

            chartData = GetDataSummary(chartData);

            chartDataResult = AddChartDataToResultList(chartData, chartDataResult);

            chartData.Clear();

            foreach (var item in gropuedSoldInvoiceSum)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "soldinvoices",
                    DataValue = item.total,
                    Month = item.month,
                });
            }
            chartData = GetDataSummary(chartData);

            chartDataResult = AddChartDataToResultList(chartData, chartDataResult);

            return chartDataResult;
        }

        private List<ChartData> AddChartDataToResultList(List<ChartData> data, List<ChartData> result)
        {
            data = data.OrderBy(m => m.Month).ToList();

            foreach (var item in data)
            {
                result.Add(item);
            }

            return result;
        }

        private List<ChartData> GetDataSummary(List<ChartData> modeldata)
        {
            string chartType = string.Empty;

            if (modeldata.Count() > 0)
            {
                chartType = modeldata.Where(m => m.Month != 0).FirstOrDefault().ChartType;
            }

            for (int i = 1; i <= 12; i++)
            {
                if (modeldata.Any(m => m.Month == i))
                {

                }
                else
                {
                    modeldata.Add(new ChartData
                    {
                        ChartType = chartType,
                        DataValue = 0,
                        DateValue = DateTime.Now,
                        Month = i,
                    });
                }
            }

            return modeldata;
        }


        private List<ChartData> GetMonthlyChartData(string userid)
        {
            var chartData = new List<ChartData>();

            var currentMonth = DateTime.Now.ToString("MM");

            var invoiceData = new List<InvoiceModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                invoiceData = dbContext.Invoices.Where(m => m.ProfileId == userid).ToList();
            }

            var groupedTotalInvoiceSum = invoiceData.Where(s => s.AdminStatus == "approved" && s.Verified == true && s.Date_Created.ToString("MM") == currentMonth).GroupBy(m => m.Date_Created, (key, value) => new { day = key, total = value.Count() }).OrderBy(z => z.day).ToList();

            var gropuedSoldInvoiceSum = invoiceData.Where(s => s.UserStatus == "sold" && s.AdminStatus == "approved" && s.Date_Created.ToString("MM") == currentMonth).GroupBy(m => m.Date_Created, (key, value) => new { day = key, total = value.Count() }).OrderBy(z => z.day).ToList();

            foreach (var item in groupedTotalInvoiceSum)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "numberofinvoices",
                    DataValue = item.total,
                    DateValue = item.day
                });
            }

            foreach (var item in gropuedSoldInvoiceSum)
            {
                chartData.Add(new ChartData
                {
                    ChartType = "soldinvoices",
                    DataValue = item.total,
                    DateValue = item.day
                });
            }

            return chartData;
        }

        private List<ChartData> FormatThisMonthChartData(List<ChartData> model)
        {
            var countdata = model.Where(m => m.ChartType == "numberofinvoices").ToList();

            var summodel = model.Where(m => m.ChartType == "soldinvoices").ToList();

            summodel = FormateDate(summodel);

            countdata = FormateDate(countdata);

            var result = new List<ChartData>();

            result.AddRange(summodel);
            result.AddRange(countdata);

            return result;
        }

        private DateTime[] GetMonthlyLabels(List<ChartData> model)
        {
            var countdata = model.Where(m => m.ChartType == "numberofinvoices").ToList();

            var summodel = model.Where(m => m.ChartType == "soldinvoices").ToList();

            summodel = FormateDate(summodel);

            countdata = FormateDate(countdata);

            var datelables = new List<DateTime>();

            var joinsumandcount = summodel.Join(countdata, s => s.DateValue, c => c.DateValue, (s, c) => new { date = s.DateValue }).ToList();

            foreach (var item in joinsumandcount)
            {
                datelables.Add(item.date);
            }

            return datelables.ToArray();
        }

        private List<ChartData> FormateDate(List<ChartData> model)
        {
            var currentYear = DateTime.Now.Year;

            var currentmonth = DateTime.Now.Month;

            var dates = GetDates(currentYear, currentmonth).ToArray();

            var chartData = new ChartData();

            if (model.Count > 0)
            {
                chartData = model.FirstOrDefault();
            }

            for (int i = 0; i < dates.Count(); i++)
            {
                if (model.Any(m => m.DateValue.Date == dates[i].Date))
                {

                }
                else
                {

                    model.Add(new ChartData
                    {
                        ChartType = chartData.ChartType,
                        DataValue = 0,
                        DateValue = dates[i],
                        Month = currentmonth,
                    });
                }
            }

            foreach (var item in model)
            {
                item.DateValue = item.DateValue.Date;
                item.Month = item.DateValue.Month;
            }

            return model.OrderBy(m => m.DateValue).ToList();
        }

        private List<DateTime> GetDates(int year, int month)
        {
            var dates = Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                              .Select(day => new DateTime(year, month, day))
                              .ToList();

            return dates;
        }

    }
}
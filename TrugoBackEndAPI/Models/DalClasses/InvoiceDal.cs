﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TrugoBackEndAPI.Models.DatabaseModelss;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Models.DalClasses
{
    public class InvoiceDal : UserAccountDal
    {

        public List<InvoiceResponseModel> GetAllInvoices()
        {
            var invoices = new List<InvoiceModel>();

            var invoiceResponse = new List<InvoiceResponseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                invoices = dbContext.Invoices.Include("Profile").Select(m => m).ToList();

                foreach (var item in invoices)
                {
                    invoiceResponse.Add(new InvoiceResponseModel
                    {
                        Id = item.Id,
                        AdminStatus = item.AdminStatus,
                        Amount = item.Amount,
                        Client = item.Client,
                        Comment = item.Comment,
                        Date_Created = item.Date_Created,
                        DiscountRate = item.DiscountRate,
                        DueDate = item.DueDate,
                        Invitations = item.Invitations,
                        InvoiceDate = item.InvoiceDate,
                        SalePrice = item.SalePrice,
                        UserStatus = item.UserStatus,
                        InvoiceGuid = item.InvoiceGuid,
                        InvoiceNumber = item.InvoiceNumber,
                        InvoicePath = item.InvoicePath,
                        InvoiceTitle = item.InvoiceTitle,
                        IsFeatured = item.IsFeatured,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,
                        },
                        ProfileId = item.ProfileId,
                        Verified = item.Verified

                    });
                }
            }

            return invoiceResponse;

        }

        public async Task<bool> VerifyInvoiceAsync(InvoiceRequestModel model)
        {
            bool isSuccessful = false;
            int result = 0;
            using (var dbContext = new ApplicationDbContext())
            {
                var invoice = dbContext.Invoices.SingleOrDefault(m => m.Id == model.Invoiceid && m.ProfileId == model.UserId && m.UserStatus == "on sale");

                if (invoice != null)
                {
                    invoice.Verified = true;

                    result = await dbContext.SaveChangesAsync();
                }
            }

            if (result > 0)
            {
                isSuccessful = true;
            }

            return isSuccessful;
        }

        public async Task<bool> RevokeInvoiceVerification(InvoiceRequestModel model)
        {
            bool isSuccessful = false;
            int result = 0;
            using (var dbContext = new ApplicationDbContext())
            {
                var invoice = dbContext.Invoices.SingleOrDefault(m => m.Id == model.Invoiceid && m.ProfileId == model.UserId && m.UserStatus == "on sale");
                if (invoice != null)
                {
                    invoice.Verified = false;

                    result = await dbContext.SaveChangesAsync();
                }
            }

            if (result > 1)
            {
                isSuccessful = true;
            }

            return isSuccessful;
        }

        public async Task<bool> ApproveInvoiceAsync(InvoiceRequestModel model)
        {
            bool isSuccessful = false;
            int result = 0;
            using (var dbContext = new ApplicationDbContext())
            {
                var invoice = dbContext.Invoices.SingleOrDefault(m => m.Id == model.Invoiceid && m.ProfileId == model.UserId && m.UserStatus == "on sale");
                if (invoice != null && invoice?.Verified == true)
                {
                    invoice.AdminStatus = "approved";

                    result = await dbContext.SaveChangesAsync();
                }
            }

            await SendInvoiceModerationEmailAsync(model.UserId, model.Invoiceid, true);

            AddNotification(model.UserId, "Invoice has been approved", DateTime.Now, false);


            if (result > 1)
            {
                isSuccessful = true;
            }

            return isSuccessful;
        }

        public async Task<bool> ModerateInvoice(InvoiceRequestModel model)
        {
            bool isSuccessful = false;
            int result = 0;
            using (var dbContext = new ApplicationDbContext())
            {
                var invoice = dbContext.Invoices.SingleOrDefault(m => m.Id == model.Invoiceid && m.InvoiceGuid == model.Code && m.ProfileId == model.UserId && m.UserStatus == "on sale");
                if (invoice != null)
                {
                    invoice.AdminStatus = "not approved";

                    result = await dbContext.SaveChangesAsync();
                }
            }

            AddNotification(model.UserId, "Invoice has been put on moderation by TruGo team", DateTime.Now, false);


            if (result > 0)
            {
                isSuccessful = true;
            }

            return isSuccessful;
        }

        public async Task<bool> DeclineInvoice(InvoiceRequestModel model)
        {
            bool isSuccessful = false;
            int result = 0;
            using (var dbContext = new ApplicationDbContext())
            {
                var invoice = dbContext.Invoices.SingleOrDefault(m => m.Id == model.Invoiceid && m.InvoiceGuid == model.Code && m.ProfileId == model.UserId && m.UserStatus == "on sale");

                if (invoice != null)
                {
                    invoice.AdminStatus = "not approved";

                    result = await dbContext.SaveChangesAsync();
                }
            }

            await SendInvoiceModerationEmailAsync(model.UserId, model.Invoiceid, false);

            AddNotification(model.UserId, "Invoice has been declined", DateTime.Now, false);

            if (result > 0)
            {
                isSuccessful = true;
            }

            return isSuccessful;
        }

        public InvoiceDetailResponseModel GetInvoiceDetail(int? id, string code)
        {
            var invoiceDetail = new InvoiceDetailResponseModel();

            if (id != null)
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    var invoice = dbContext.Invoices.Include("Payments").SingleOrDefault(m => m.Id == id && m.InvoiceGuid.ToString() == code);

                    if (invoice != null)
                    {
                        invoiceDetail.Invoice.AdminStatus = invoice.AdminStatus;
                        invoiceDetail.Invoice.Amount = invoice.Amount;
                        invoiceDetail.Invoice.Client = invoice.Client;
                        invoiceDetail.Invoice.Comment = invoice.Comment;
                        invoiceDetail.Invoice.Date_Created = invoice.Date_Created;
                        invoiceDetail.Invoice.DiscountRate = invoice.DiscountRate;
                        invoiceDetail.Invoice.DueDate = invoice.DueDate;
                        invoiceDetail.Invoice.Id = invoice.Id;
                        invoiceDetail.Invoice.Invitations = invoice.Invitations;
                        invoiceDetail.Invoice.InvoiceDate = invoice.InvoiceDate;
                        invoiceDetail.Invoice.InvoiceGuid = invoice.InvoiceGuid;
                        invoiceDetail.Invoice.InvoiceNumber = invoice.InvoiceNumber;
                        invoiceDetail.Invoice.InvoicePath = invoice.InvoicePath;
                        invoiceDetail.Invoice.InvoiceTitle = invoice.InvoiceTitle;
                        invoiceDetail.Invoice.IsFeatured = invoice.IsFeatured;
                        invoiceDetail.Invoice.ProfileId = invoice.ProfileId;
                        invoiceDetail.Invoice.SalePrice = invoice.SalePrice;
                        invoiceDetail.Invoice.UserStatus = invoice.UserStatus;
                        invoiceDetail.Invoice.Verified = invoice.Verified;

                        foreach (var item in invoice.Payments)
                        {
                            invoiceDetail.Payments.Add(new PaymentResponseModel
                            {
                                Id = item.Id,
                                Amount = item.Amount,
                                Date_Created = item.Date_Created,
                                InvoiceId = item.InvoiceId,
                                InvoicePercentage = item.InvoicePercentage,
                                ProfileId = item.ProfileId
                            });
                        }
 
                    }
                }
            }

            return invoiceDetail;
        }
    }
}
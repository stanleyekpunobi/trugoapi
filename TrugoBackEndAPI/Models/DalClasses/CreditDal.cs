﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DatabaseModelss;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Models.DalClasses
{
    public class CreditDal : UserAccountDal
    {
        #region withdrawal
        public async Task<bool> CreateWithdrawalRequest(WithdrawCreditModel model)
        {
            bool isSuccessful = false;
            var withdrawCreditRequestModel = new WithdrawCreditModel();
            using (var dbContext = new ApplicationDbContext())
            {
                var user = dbContext.Users.Find(model.ProfileId);

                if (user.Profiles.TotalCredit < model.Amount)
                {

                }
                else
                {
                    withdrawCreditRequestModel = dbContext.WithdrawCredit.Add(model);
                    dbContext.ConfigureUsername(model.ProfileId);
                    await dbContext.SaveChangesAsync();
                }

                isSuccessful = true;
            }

            return isSuccessful;
        }

        public List<WithdrawCreditResponse> GetAllWithdrawalRequests()
        {
            var requests = new List<WithdrawCreditModel>();

            var withdrawalResponse = new List<WithdrawCreditResponse>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.WithdrawCredit.Include("Profile").Select(m => m).OrderByDescending(m => m.Date_Created).ToList();

                foreach (var item in requests)
                {
                    withdrawalResponse.Add(new WithdrawCreditResponse
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Estimate = item.Date_Estimate,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,
                        },
                        Status = item.Status,
                        Verified = item.Verified
                    });
                }
            }

            return withdrawalResponse;
        }

        public List<WithdrawCreditResponse> GetPendingWithdrawalRequests()
        {
            var requests = new List<WithdrawCreditModel>();

            var withdrawalResponse = new List<WithdrawCreditResponse>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.WithdrawCredit.Include("Profile").Where(m => m.Verified == false && m.Status == false).OrderByDescending(m => m.Date_Created).ToList();

                foreach (var item in requests)
                {
                    withdrawalResponse.Add(new WithdrawCreditResponse
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Estimate = item.Date_Estimate,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,
                        },
                        Status = item.Status,
                        Verified = item.Verified
                    });
                }

            }

            return withdrawalResponse;
        }

        public List<WithdrawCreditResponse> GetVerifiedWithdrawalRequests()
        {
            var requests = new List<WithdrawCreditModel>();

            var withdrawalResponse = new List<WithdrawCreditResponse>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.WithdrawCredit.Include("Profile").Where(m => m.Verified == true).OrderByDescending(m => m.Date_Created).ToList();

                foreach (var item in requests)
                {
                    withdrawalResponse.Add(new WithdrawCreditResponse
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Estimate = item.Date_Estimate,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,
                        },
                        Status = item.Status,
                        Verified = item.Verified
                    });
                }
            }

            return withdrawalResponse;
        }

        public List<WithdrawCreditResponse> GetApprovedWithdrawalRequests()
        {
            var requests = new List<WithdrawCreditModel>();

            var withdrawalResponse = new List<WithdrawCreditResponse>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.WithdrawCredit.Include("Profile").Where(m => m.Status == true).OrderByDescending(m => m.Date_Created).ToList();

                foreach (var item in requests)
                {
                    withdrawalResponse.Add(new WithdrawCreditResponse
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Estimate = item.Date_Estimate,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,
                        },
                        Status = item.Status,
                        Verified = item.Verified
                    });
                }
            }

            return withdrawalResponse;
        }

        public WithdrawCreditResponse GetWithdrawalRequest(CreditLibraryRequestModel model)
        {
            var request = new WithdrawCreditModel();

            var withdrawalResponse = new WithdrawCreditResponse();

            using (var dbContext = new ApplicationDbContext())
            {
                request = dbContext.WithdrawCredit.Include("Profile").SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid);

                withdrawalResponse.Amount = request.Amount;
                withdrawalResponse.Date_Created = request.Date_Created;
                withdrawalResponse.Date_Estimate = request.Date_Estimate;
                withdrawalResponse.Id = request.Id;
                withdrawalResponse.Profile = new UserprofileData()
                {
                    UserId = request.Profile.Id,
                    EmailAddress = request.Profile.ApplicationUser.Email,
                    FirstName = request.Profile.FirstName,
                    LastName = request.Profile.LastName,
                    BusinessName = request.Profile.BusinessName,
                    IsAccountApproved = request.Profile.ApplicationUser.AccountApproved,
                    IsEmailConfirmed = request.Profile.ApplicationUser.EmailConfirmed,
                    TotalCredit = request.Profile.TotalCredit,
                };
                withdrawalResponse.Status = request.Status;
                withdrawalResponse.Verified = request.Verified;

            }

            return withdrawalResponse;
        }

        public async Task<bool> ApproveWithdrawalRequest(CreditLibraryRequestModel model)
        {

            bool isSuccessful = false;

            int statuschange = 0;

            var request = new WithdrawCreditModel();

            try
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    request = dbContext.WithdrawCredit.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && m.Verified == true && m.Amount == model.CreditAmount);

                    if (!string.IsNullOrEmpty(request.ProfileId))
                    {
                        request.Status = true;
                        dbContext.ConfigureUsername(model.Userid);
                        statuschange = await dbContext.SaveChangesAsync();
                    }
                }

                AddNotification(model.Userid, "Your TruGo credit withdrawal request has been approved", DateTime.Now, false);


                var user = ApplyWithdrawalRequestPolicy(model.Userid, request.Amount);

                if (statuschange == 1 && user != null)
                {
                    isSuccessful = await AddToIncomeStatement(request, "credit request", "credit request approval", user.TotalCredit);
                    SendWithdrawalApprovalEmail(user);
                }
                else
                {
                    isSuccessful = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        public async Task<bool> VerifyWithdrawalRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            using (var dbContext = new ApplicationDbContext())
            {
                var request = dbContext.WithdrawCredit.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && m.Verified != true && m.Amount == model.CreditAmount);

                if (request != null)
                {
                    request.Verified = true;
                    dbContext.ConfigureUsername(model.Userid);
                    statuschange = await dbContext.SaveChangesAsync();
                }

            }
            if (statuschange == 1)
            {
                isSuccessful = true;

            }
            else { }

            return isSuccessful;
        }

        public async Task<bool> DeclineWithdrawalRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            using (var dbContext = new ApplicationDbContext())
            {
                var request = dbContext.WithdrawCredit.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && m.Amount == model.CreditAmount);

                if (request != null)
                {
                    request.Status = false;
                    dbContext.ConfigureUsername(model.Userid);
                    statuschange = await dbContext.SaveChangesAsync();
                }
            }

            if (statuschange == 1)
            {
                isSuccessful = true;
                SendWithdrawalDeclineEmail(model.Userid);
            }
            else
            {
                isSuccessful = false;
            }


            return isSuccessful;
        }

        public async Task<bool> RevokeVerifiedWithdrawalRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            using (var dbContext = new ApplicationDbContext())
            {
                var request = dbContext.WithdrawCredit.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && (m.Amount == model.CreditAmount));

                if (request != null)
                {
                    request.Verified = false;
                    dbContext.ConfigureUsername(model.Userid);
                    statuschange = await dbContext.SaveChangesAsync();
                }
            }

            if (statuschange == 1)
            {
                isSuccessful = true;
            }
            else
            {
                isSuccessful = false;
            }

            return isSuccessful;
        }

        #endregion

        #region credit requests
        public async Task<bool> CreateCreditRequest(CreditRequestModel model)
        {
            bool isSuccessful = false;
            var creditRequestModel = new CreditRequestModel();
            using (var dbContext = new ApplicationDbContext())
            {
                creditRequestModel = dbContext.CreditRequest.Add(model);
                dbContext.ConfigureUsername(model.ProfileId);
                await dbContext.SaveChangesAsync();
                isSuccessful = true;
            }

            //isSuccessful = await AddToIncomeStatement(model, "credit", "credit purchase");

            return isSuccessful;
        }

        public List<CreditResponseModel> GetAllCreditRequests()
        {
            var requests = new List<CreditRequestModel>();

            var creditResponse = new List<CreditResponseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.CreditRequest.Include("Profile").Select(m => m).OrderByDescending(m => m.Date_Created).ToList();

                foreach (var item in requests)
                {
                    creditResponse.Add(new CreditResponseModel
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Paid = item.Date_Paid,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,

                        },
                        Status = item.Status,
                        Verified = item.Verified,
                        Channel = item.Channel,
                        PaymentRef = item.PaymentRef
                    });
                }
            }

            return creditResponse;
        }

        public List<CreditResponseModel> GetPendingCreditRequests()
        {
            var requests = new List<CreditRequestModel>();

            var creditResponse = new List<CreditResponseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.CreditRequest.Include("Profile").Where(m => m.Verified == true && m.Status == false).ToList();

                foreach (var item in requests)
                {
                    creditResponse.Add(new CreditResponseModel
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Paid = item.Date_Paid,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,

                        },
                        Status = item.Status,
                        Verified = item.Verified,
                        Channel = item.Channel,
                        PaymentRef = item.PaymentRef
                    });
                }
            }

            return creditResponse;
        }

        public List<CreditResponseModel> GetApprovedCreditRequests()
        {
            var requests = new List<CreditRequestModel>();

            var creditResponse = new List<CreditResponseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.CreditRequest.Include("Profile").Where(m => m.Status == true).ToList();

                foreach (var item in requests)
                {
                    creditResponse.Add(new CreditResponseModel
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Paid = item.Date_Paid,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,

                        },
                        Status = item.Status,
                        Verified = item.Verified,
                        Channel = item.Channel,
                        PaymentRef = item.PaymentRef
                    });
                }
            }

            return creditResponse;
        }

        public CreditResponseModel GetCreditRequest(CreditLibraryRequestModel model)
        {
            var request = new CreditRequestModel();

            var creditResponse = new CreditResponseModel();


            using (var dbContext = new ApplicationDbContext())
            {
                request = dbContext.CreditRequest.Include("Profile").SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid);

                creditResponse = new CreditResponseModel()
                {
                    Amount = request.Amount,
                    Channel = request.Channel,
                    Date_Created = request.Date_Created,
                    Date_Paid = request.Date_Paid,
                    Id = request.Id,
                    PaymentRef = request.PaymentRef,
                    Profile = new UserprofileData()
                    {
                        UserId = request.Profile.Id,
                        EmailAddress = request.Profile.ApplicationUser.Email,
                        FirstName = request.Profile.FirstName,
                        LastName = request.Profile.LastName,
                        BusinessName = request.Profile.BusinessName,
                        IsAccountApproved = request.Profile.ApplicationUser.AccountApproved,
                        IsEmailConfirmed = request.Profile.ApplicationUser.EmailConfirmed,
                        TotalCredit = request.Profile.TotalCredit,
                    },
                    Status = request.Status,
                    Verified = request.Verified
                };
            }

            return creditResponse;
        }

        public async Task<bool> ApproveCreditRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            var request = new CreditRequestModel();

            using (var dbContext = new ApplicationDbContext())
            {
                request = dbContext.CreditRequest.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && m.Verified == true && m.Amount == model.CreditAmount);

                if (!string.IsNullOrEmpty(request.ProfileId))
                {
                    request.Status = true;
                    dbContext.ConfigureUsername(model.Userid);
                    statuschange = await dbContext.SaveChangesAsync();
                }
            }

            AddNotification(model.Userid, "Your TruGo credit request has been approved", DateTime.Now, false);

            var user = ApplyCreditRequestPolicy(model.Userid, model.CreditAmount);

            if (statuschange == 1 && user != null)
            {
                isSuccessful = await AddToIncomeStatement(request, "credit request", "credit request approval", user.TotalCredit);
                SendCreditApprovalEmail(user);
            }
            else
            {
                isSuccessful = false;
            }

            return isSuccessful;
        }

        public async Task<bool> VerifyCreditRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            using (var dbContext = new ApplicationDbContext())
            {
               var request = dbContext.CreditRequest.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && m.Verified != true);

                if (request != null)
                {
                    request.Verified = true;
                    dbContext.ConfigureUsername(model.Userid);
                    statuschange = await dbContext.SaveChangesAsync();
                }
            }

            if (statuschange == 1)
            {
                isSuccessful = true;

            }

            return isSuccessful;
        }

        public async Task<bool> DeclineCreditRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            using (var dbContext = new ApplicationDbContext())
            {
                var request = dbContext.CreditRequest.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid);

                if (request != null)
                {
                    request.Status = false;

                    dbContext.ConfigureUsername(model.Userid);

                    statuschange = await dbContext.SaveChangesAsync();
                }
            }

            if (statuschange == 1)
            {
                isSuccessful = true;
                SendCreditDeclineEmail(model.Userid);
            }
            else
            {
                isSuccessful = false;
            }
                                  
            return isSuccessful;
        }

        public async Task<bool> RevokeVerifiedCreditRequest(CreditLibraryRequestModel model)
        {
            bool isSuccessful = false;

            int statuschange = 0;

            using (var dbContext = new ApplicationDbContext())
            {
                var request = dbContext.CreditRequest.SingleOrDefault(m => m.Id == model.Id && m.ProfileId == model.Userid && (m.Amount == model.CreditAmount));

                if (request != null)
                {
                    dbContext.ConfigureUsername(model.Userid);
                    request.Verified = false;
                    statuschange = await dbContext.SaveChangesAsync();
                }
            }

            if (statuschange == 1)
            {
                isSuccessful = true;
            }
            else
            {
                isSuccessful = false;
            }


            return isSuccessful;
        }

        public List<CreditResponseModel> GetVerifiedCreditRequests()
        {
            var requests = new List<CreditRequestModel>();

            var creditResponse = new List<CreditResponseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                requests = dbContext.CreditRequest.Include("Profile").Where(m => m.Verified == true).OrderByDescending(m => m.Date_Created).ToList();

                foreach (var item in requests)
                {
                    creditResponse.Add(new CreditResponseModel
                    {
                        Amount = item.Amount,
                        Date_Created = item.Date_Created,
                        Date_Paid = item.Date_Paid,
                        Id = item.Id,
                        Profile = new UserprofileData()
                        {
                            UserId = item.Profile.Id,
                            EmailAddress = item.Profile.ApplicationUser.Email,
                            FirstName = item.Profile.FirstName,
                            LastName = item.Profile.LastName,
                            BusinessName = item.Profile.BusinessName,
                            IsAccountApproved = item.Profile.ApplicationUser.AccountApproved,
                            IsEmailConfirmed = item.Profile.ApplicationUser.EmailConfirmed,
                            TotalCredit = item.Profile.TotalCredit,
                        },
                        Status = item.Status,
                        Verified = item.Verified,
                        Channel = item.Channel,
                        PaymentRef = item.PaymentRef
                    });
                }
            }

            return creditResponse;
        }
        
        #endregion
    }
}
﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DatabaseModelss;

namespace TrugoBackEndAPI.Models.DalClasses
{
    public class UserAccountDal : NotificationHelperClass
    {
        public async Task<bool> SetupUserProfile(AccountSetupModel model)
        {
            bool createProfile = false;
            using (var dbContext = new ApplicationDbContext())
            {
                var profile = dbContext.Profiles.Find(model.Id);
                if (profile == null)
                {
                    profile = dbContext.Profiles.Add(model);
                    dbContext.ConfigureUsername(model.Id);

                    await dbContext.SaveChangesAsync();
                    if (!string.IsNullOrEmpty(profile.Id))
                    {
                        createProfile = true;
                    }
                }
                else
                {

                }

            }

            return createProfile;

        }

        public AccountSetupModel ApplyWithdrawalRequestPolicy(string userid, int credit)
        {
            var user = new AccountSetupModel();
            bool isSuccessful = false;
            using (var dbContext = new ApplicationDbContext())
            {
                user = dbContext.Profiles.Find(userid);

                if (!string.IsNullOrEmpty(user.Id) && user.ApplicationUser.AccountApproved && user.TotalCredit >= credit)
                {
                    user.TotalCredit = user.TotalCredit - credit;
                    dbContext.ConfigureUsername(userid);

                    dbContext.SaveChanges();

                    isSuccessful = true;
                }
               
            }

            if (isSuccessful == true)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public AccountSetupModel ApplyCreditRequestPolicy(string userid, int credit)
        {
            var user = new AccountSetupModel();
            bool isSuccessful = false;
            using (var dbContext = new ApplicationDbContext())
            {
                user = dbContext.Profiles.Find(userid);

                if (!string.IsNullOrEmpty(user.Id) && user.ApplicationUser.AccountApproved)
                {
                    user.TotalCredit = user.TotalCredit + credit;

                    dbContext.ConfigureUsername(userid);

                    dbContext.SaveChanges();

                    isSuccessful = true;
                }

            }

            if (isSuccessful == true)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public AccountSetupModel GetUserProfile(string userid)
        {
            var profile = new AccountSetupModel();

            using (var dbContext = new ApplicationDbContext())
            {
                profile = dbContext.Profiles.Include("AspNetUsers").SingleOrDefault(m => m.Id == userid);
            }

            return profile;
        }

        public bool CheckProfile(string userid)
        {
            bool status = false;
            using (var dbContext = new ApplicationDbContext())
            {
                var profile = dbContext.Profiles.Find(userid);
                if (profile != null)
                {
                    status = true;
                }
                else
                {

                }
            }

            return status;
        }

        public async Task<bool> AddToIncomeStatement(CreditRequestModel model, string type, string description, int balance)
        {
            var incomeStatementModel = new IncomeStatementModel()
            {
                Amount = model.Amount,
                ProfileId = model.ProfileId,
                Type = type,
                Description = description,
                Balance = balance
            };

            using (var dbContext = new ApplicationDbContext())
            {
                var addIncomeStatement = dbContext.IncomeStatement.Add(incomeStatementModel);

                dbContext.ConfigureUsername(model.ProfileId);

                await dbContext.SaveChangesAsync();

                if (addIncomeStatement != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<bool> AddToIncomeStatement(WithdrawCreditModel model, string type, string description, int balance)
        {
            var incomeStatementModel = new IncomeStatementModel()
            {
                Amount = model.Amount,
                ProfileId = model.ProfileId,
                Type = type,
                Description = description,
                Balance = balance
            };

            using (var dbContext = new ApplicationDbContext())
            {
                var addIncomeStatement = dbContext.IncomeStatement.Add(incomeStatementModel);

                dbContext.ConfigureUsername(model.ProfileId);
                
                await dbContext.SaveChangesAsync();

                if (addIncomeStatement != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TrugoBackEndAPI.Models.ApiUtils;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Models.DalClasses
{
    public class AdminDal : UserAccountDal
    {
        private NotificationHelperClass _notificationHelperClass;

        public AdminDal()
        {

        }

        public AdminDal(NotificationHelperClass notificationHelperClass)
        {
        }

        public NotificationHelperClass NotificationHelperClass
        {
            get
            {
                return new NotificationHelperClass();
            }
            set
            {
                _notificationHelperClass = value;
            }
        }

        public bool DeclineUserProfile(string userid, string message)
        {
            bool confirmed = false;
            var user = new ApplicationUser();
            using (var dbContext = new ApplicationDbContext())
            {
                user = dbContext.Users.Find(userid);
                if (user != null)
                {
                    user.AccountApproved = false;
                    confirmed = true;
                    dbContext.ConfigureUsername(userid);
                    dbContext.SaveChanges();
                }
                else
                {

                }

            }
            if (confirmed == true)
            {
                NotificationHelperClass.SendDeclineEmail(user, message);
            }


            return confirmed;
        }

        public bool ApproveUserProfile(string userid)
        {

            bool confirmed = false;
            var user = new ApplicationUser();
            using (var dbContext = new ApplicationDbContext())
            {
                user = dbContext.Users.Find(userid);
                if (user != null && user?.EmailConfirmed == true)
                {
                    user.AccountApproved = true;
                    confirmed = true;
                    dbContext.ConfigureUsername(userid);
                    dbContext.SaveChanges();
                }
                else
                {

                }

            }

            AddNotification(userid, "Your Trugo user profile has been approved", DateTime.Now, false);

            if (confirmed == true)
            {
                NotificationHelperClass.SendApprovalEmail(user);
            }


            return confirmed;
        }

        public bool VerifyUserProfile(string userid)
        {
            bool confirmed = false;
            var user = new ApplicationUser();
            using (var dbContext = new ApplicationDbContext())
            {
                user = dbContext.Users.Find(userid);
                if (user != null && user?.EmailConfirmed == true)
                {
                    user.AccountVerified = true;
                    confirmed = true;
                    dbContext.ConfigureUsername(userid);
                    dbContext.SaveChanges();
                }
                else
                {

                }
            }

            return confirmed;
        }

        public List<UserprofileData> GetVerifiedUsers()
        {
            var users = new List<ApplicationUser>();

            var profiles = new List<UserprofileData>();

            using (var dbContext = new ApplicationDbContext())
            {
                users = dbContext.Users.Include("Roles").Include("Profiles").Where(m => m.AccountVerified == true).ToList();
            }

            if (users.Count() != 0)
            {
                foreach (var user in users)
                {
                    if (user.Profiles == null)
                    {

                    }
                    else
                    {
                        profiles.Add(new UserprofileData
                        {
                            EmailAddress = user.Email,
                            IsAccountApproved = user.AccountApproved,
                            IsEmailConfirmed = user.EmailConfirmed,
                            Address = user.Profiles.Address,
                            BusinessName = user?.Profiles.BusinessName,
                            BusinessSpan = user.Profiles.BusinessSpan,
                            City = user?.Profiles.City,
                            COI = user?.Profiles.COI,
                            Country = user?.Profiles.Country,
                            Date_Created = user.Profiles.Date_Created,
                            EstimatedAnnualRevenue = user?.Profiles.EstimatedAnnualRevenue,
                            FirstName = user?.Profiles.FirstName,
                            LastName = user?.Profiles.LastName,
                            Gender = user?.Profiles.Gender,
                            HowDidYouHearAboutUs = user?.Profiles.HowDidYouHearAboutUs,
                            POD = user?.Profiles.POD,
                            PrincipalClient = user?.Profiles.PrincipalClient,
                            ReferralName = user?.Profiles.ReferralName,
                            Industry = user?.Profiles.Industry,
                            InvestorType = user?.Profiles.InvestorType,
                            InvoiceRange = user?.Profiles.InvoiceRange,
                            MiddleName = user?.Profiles.MiddleName,
                            MOI = user?.Profiles.MOI,
                            MOU = user?.Profiles.MOU,
                            PhoneNumber = user?.Profiles.PhoneNumber,
                            TotalCredit = user.Profiles.TotalCredit,
                            UserId = user.Id,
                        });
                    }
                }
            }

            return profiles;
        }

        public List<UserprofileData> GetPendingUsers()
        {
            var users = new List<ApplicationUser>();

            var profiles = new List<UserprofileData>();

            using (var dbContext = new ApplicationDbContext())
            {
                users = dbContext.Users.Include("Roles").Include("Profiles").Where(m => m.AccountApproved != true && m.AccountVerified != true).ToList();
            }

            if (users.Count() != 0)
            {
                foreach (var user in users)
                {
                    if (user.Profiles == null)
                    {

                    }
                    else
                    {
                        profiles.Add(new UserprofileData
                        {
                            EmailAddress = user.Email,
                            IsAccountApproved = user.AccountApproved,
                            IsEmailConfirmed = user.EmailConfirmed,
                            Address = user.Profiles.Address,
                            BusinessName = user?.Profiles.BusinessName,
                            BusinessSpan = user.Profiles.BusinessSpan,
                            City = user?.Profiles.City,
                            COI = user?.Profiles.COI,
                            Country = user?.Profiles.Country,
                            Date_Created = user.Profiles.Date_Created,
                            EstimatedAnnualRevenue = user?.Profiles.EstimatedAnnualRevenue,
                            FirstName = user?.Profiles.FirstName,
                            LastName = user?.Profiles.LastName,
                            Gender = user?.Profiles.Gender,
                            HowDidYouHearAboutUs = user?.Profiles.HowDidYouHearAboutUs,
                            POD = user?.Profiles.POD,
                            PrincipalClient = user?.Profiles.PrincipalClient,
                            ReferralName = user?.Profiles.ReferralName,
                            Industry = user?.Profiles.Industry,
                            InvestorType = user?.Profiles.InvestorType,
                            InvoiceRange = user?.Profiles.InvoiceRange,
                            MiddleName = user?.Profiles.MiddleName,
                            MOI = user?.Profiles.MOI,
                            MOU = user?.Profiles.MOU,
                            PhoneNumber = user?.Profiles.PhoneNumber,
                            TotalCredit = user.Profiles.TotalCredit,
                            UserId = user.Id,
                        });
                    }
                }
            }

            return profiles;
        }

        public List<UserprofileData> GetApprovedUsers()
        {
            var users = new List<ApplicationUser>();

            var profiles = new List<UserprofileData>();

            using (var dbContext = new ApplicationDbContext())
            {
                users = dbContext.Users.Include("Roles").Include("Profiles").Where(m => m.AccountApproved != false).ToList();
            }

            if (users.Count() != 0)
            {
                foreach (var user in users)
                {
                    if (user.Profiles == null)
                    {

                    }
                    else
                    {
                        profiles.Add(new UserprofileData
                        {
                            EmailAddress = user.Email,
                            IsAccountApproved = user.AccountApproved,
                            IsEmailConfirmed = user.EmailConfirmed,
                            Address = user.Profiles.Address,
                            BusinessName = user?.Profiles.BusinessName,
                            BusinessSpan = user.Profiles.BusinessSpan,
                            City = user?.Profiles.City,
                            COI = user?.Profiles.COI,
                            Country = user?.Profiles.Country,
                            Date_Created = user.Profiles.Date_Created,
                            EstimatedAnnualRevenue = user?.Profiles.EstimatedAnnualRevenue,
                            FirstName = user?.Profiles.FirstName,
                            LastName = user?.Profiles.LastName,
                            Gender = user?.Profiles.Gender,
                            HowDidYouHearAboutUs = user?.Profiles.HowDidYouHearAboutUs,
                            POD = user?.Profiles.POD,
                            PrincipalClient = user?.Profiles.PrincipalClient,
                            ReferralName = user?.Profiles.ReferralName,
                            Industry = user?.Profiles.Industry,
                            InvestorType = user?.Profiles.InvestorType,
                            InvoiceRange = user?.Profiles.InvoiceRange,
                            MiddleName = user?.Profiles.MiddleName,
                            MOI = user?.Profiles.MOI,
                            MOU = user?.Profiles.MOU,
                            PhoneNumber = user?.Profiles.PhoneNumber,
                            TotalCredit = user.Profiles.TotalCredit,
                            UserId = user.Id,
                        });
                    }
                }
            }

            return profiles;
        }

        public List<UserprofileData> GetAllUsers()
        {
            var users = new List<ApplicationUser>();

            var profiles = new List<UserprofileData>();

            using (var dbContext = new ApplicationDbContext())
            {
                users = dbContext.Users.Include("Roles").Include("Profiles").Select(m => m).ToList();
            }

            if (users.Count() != 0)
            {
                foreach (var user in users)
                {
                    if (user.Profiles == null)
                    {

                    }
                    else
                    {
                        profiles.Add(new UserprofileData
                        {
                            EmailAddress = user.Email,
                            IsAccountApproved = user.AccountApproved,
                            IsEmailConfirmed = user.EmailConfirmed,
                            Address = user.Profiles.Address,
                            BusinessName = user?.Profiles.BusinessName,
                            BusinessSpan = user.Profiles.BusinessSpan,
                            City = user?.Profiles.City,
                            COI = user?.Profiles.COI,
                            Country = user?.Profiles.Country,
                            Date_Created = user.Profiles.Date_Created,
                            EstimatedAnnualRevenue = user?.Profiles.EstimatedAnnualRevenue,
                            FirstName = user?.Profiles.FirstName,
                            LastName = user?.Profiles.LastName,
                            Gender = user?.Profiles.Gender,
                            HowDidYouHearAboutUs = user?.Profiles.HowDidYouHearAboutUs,
                            POD = user?.Profiles.POD,
                            PrincipalClient = user?.Profiles.PrincipalClient,
                            ReferralName = user?.Profiles.ReferralName,
                            Industry = user?.Profiles.Industry,
                            InvestorType = user?.Profiles.InvestorType,
                            InvoiceRange = user?.Profiles.InvoiceRange,
                            MiddleName = user?.Profiles.MiddleName,
                            MOI = user?.Profiles.MOI,
                            MOU = user?.Profiles.MOU,
                            PhoneNumber = user?.Profiles.PhoneNumber,
                            TotalCredit = user.Profiles.TotalCredit,
                            UserId = user.Id,
                        });
                    }
                   
                }
            }


            return profiles;

        }

        public bool RevokeUserVerification(string userid)
        {
            var isSuccessful = false;
            using (var dbContext = new ApplicationDbContext())
            {
                var user = dbContext.Users.Find(userid);
                if (user != null)
                {
                    user.AccountVerified = false;
                    dbContext.ConfigureUsername(userid);
                    dbContext.SaveChanges();
                    isSuccessful = true;
                }
                else
                {
                    isSuccessful = false;
                }
            }

            return isSuccessful;
        }
    }


}

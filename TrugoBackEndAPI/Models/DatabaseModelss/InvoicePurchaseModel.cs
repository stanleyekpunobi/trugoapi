﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrugoBackEndAPI.Models.DatabaseModelss
{
    [TrackChanges]
    public class InvoicePurchaseModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ProfileId { get; set; }

        public AccountSetupModel Profile { get; set; }
        [Required]
        public int InvoiceId { get; set; }

        public InvoiceModel Invoice { get; set; }

        public int InvoicePercentage { get; set; }
        [Required]
        public int Amount { get; set; }

        public DateTime Date_Created { get; set; }
        [Required]
        public Guid Code { get; set; }
    }
}
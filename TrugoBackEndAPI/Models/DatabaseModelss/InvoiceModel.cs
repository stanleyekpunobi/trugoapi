﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrugoBackEndAPI.Models.DatabaseModelss
{
    [TrackChanges]
    public class InvoiceModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string InvoiceNumber { get; set; }
        [Required]
        public string InvoiceTitle { get; set; }
        [Required]
        public string Client { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public int DiscountRate { get; set; }
        [Required]
        public int SalePrice { get; set; }
        public string Comment { get; set; }
        public string Invitations { get; set; }
        public string ProfileId { get; set; }
        public AccountSetupModel Profile { get; set; }
        [Required]
        public string InvoicePath { get; set; }
        public DateTime Date_Created { get; set; }
        public string AdminStatus { get; set; }
        public bool Verified { get; set; }
        public string UserStatus { get; set; }
        public bool IsFeatured { get; set; }
        public Guid InvoiceGuid { get; set; }

        public virtual ICollection<InvoicePurchaseModel> Payments { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrugoBackEndAPI.Models.DatabaseModelss
{
    //[TrackChanges]
    public class NotificationModel
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Message { get; set; }

        public DateTime Date_Created { get; set; }

        public bool IsViewed { get; set; }

        public NotificationModel()
        {

        }

        public NotificationModel(string userId, string message, DateTime dateCreated, bool status)
        {
            UserId = userId;
            Message = message;
            Date_Created = dateCreated;
            IsViewed = status;
        }

    }
}
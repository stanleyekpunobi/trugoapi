﻿using System.ComponentModel.DataAnnotations;

namespace TrugoBackEndAPI.Models.DatabaseModelss
{
    [TrackChanges]
    public class RoleModel
    {
        public RoleModel() { }

        public RoleModel(ApplicationRole role)
        {
            Id = role.Id;
            Name = role.Name;
        }

        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
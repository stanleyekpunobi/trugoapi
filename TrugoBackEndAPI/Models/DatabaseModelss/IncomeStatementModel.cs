﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrugoBackEndAPI.Models.DatabaseModelss
{
    [TrackChanges]
    public class IncomeStatementModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Type { get; set; }
        public int Balance { get; set; }
        [Required]
        public DateTime Date_Created { get { return DateTime.Now; } set { } }
        public string ProfileId { get; set; }
        public AccountSetupModel Profile { get; set; }
    }
}
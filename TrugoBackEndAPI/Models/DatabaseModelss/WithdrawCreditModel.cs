﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TrugoBackEndAPI.Models.DatabaseModelss
{
    [TrackChanges]
    public class WithdrawCreditModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public DateTime Date_Estimate { get; set; }
        public DateTime Date_Created { get { return DateTime.Now; } set { } }
        public string ProfileId { get; set; }
        public AccountSetupModel Profile { get; set; }
        public bool Verified { get; set; }
        public bool Status { get; set; }
    }
}
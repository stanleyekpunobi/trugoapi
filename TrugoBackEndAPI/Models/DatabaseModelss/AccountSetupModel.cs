﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TrugoBackEndAPI.Models.DatabaseModelss;

namespace TrugoBackEndAPI.Models
{
    [TrackChanges]
    public class AccountSetupModel
    {
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        [Required]
        public string InvestorType { get; set; }
        public string BusinessName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Industry { get; set; }
        public string HowDidYouHearAboutUs { get; set; }
        public string InvoiceRange { get; set; }
        public int BusinessSpan { get; set; } = 0;
        public string EstimatedAnnualRevenue { get; set; }
        public string MOI { get; set; }
        public string COI { get; set; }
        public string MOU { get; set; }
        public string POD { get; set; }
        public string PrincipalClient { get; set; }
        public string ReferralName { get; set; }
        public DateTime Date_Created { get { return DateTime.Now; } set { } }
        public int TotalCredit { get; set; }
        public virtual ICollection<CreditRequestModel> CreditRequests { get; set; }
        public virtual ICollection<IncomeStatementModel> IncomeStatements { get; set; }
        public virtual ICollection<WithdrawCreditModel> WithdrawCredit { get; set; }
        public virtual ICollection<InvoiceModel> Invoices { get; set; }

        public virtual ICollection<InvoicePurchaseModel> InvoicePayments { get; set; }

    }
}
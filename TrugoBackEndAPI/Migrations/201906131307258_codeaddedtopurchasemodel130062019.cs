namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class codeaddedtopurchasemodel130062019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoicePurchaseModels", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoicePurchaseModels", "Code");
        }
    }
}

namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purchasemodel130062019 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InvoicePurchaseModels", "ProfileId", "dbo.AccountSetupModels");
            DropIndex("dbo.InvoicePurchaseModels", new[] { "ProfileId" });
            AlterColumn("dbo.InvoicePurchaseModels", "ProfileId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.String(nullable: false));
            AlterColumn("dbo.InvoicePurchaseModels", "Code", c => c.String(nullable: false));
            CreateIndex("dbo.InvoicePurchaseModels", "ProfileId");
            AddForeignKey("dbo.InvoicePurchaseModels", "ProfileId", "dbo.AccountSetupModels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoicePurchaseModels", "ProfileId", "dbo.AccountSetupModels");
            DropIndex("dbo.InvoicePurchaseModels", new[] { "ProfileId" });
            AlterColumn("dbo.InvoicePurchaseModels", "Code", c => c.String());
            AlterColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.String());
            AlterColumn("dbo.InvoicePurchaseModels", "ProfileId", c => c.String(maxLength: 128));
            CreateIndex("dbo.InvoicePurchaseModels", "ProfileId");
            AddForeignKey("dbo.InvoicePurchaseModels", "ProfileId", "dbo.AccountSetupModels", "Id");
        }
    }
}

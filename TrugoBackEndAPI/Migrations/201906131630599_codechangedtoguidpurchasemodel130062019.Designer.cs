// <auto-generated />
namespace TrugoBackEndAPI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class codechangedtoguidpurchasemodel130062019 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(codechangedtoguidpurchasemodel130062019));
        
        string IMigrationMetadata.Id
        {
            get { return "201906131630599_codechangedtoguidpurchasemodel130062019"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

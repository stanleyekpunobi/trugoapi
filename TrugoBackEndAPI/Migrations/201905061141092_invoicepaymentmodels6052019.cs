namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicepaymentmodels6052019 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvoicePurchaseModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProfileId = c.String(maxLength: 128),
                        InvoiceId = c.String(),
                        InvoicePercentage = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        Invoice_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InvoiceModels", t => t.Invoice_Id)
                .ForeignKey("dbo.AccountSetupModels", t => t.ProfileId)
                .Index(t => t.ProfileId)
                .Index(t => t.Invoice_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoicePurchaseModels", "ProfileId", "dbo.AccountSetupModels");
            DropForeignKey("dbo.InvoicePurchaseModels", "Invoice_Id", "dbo.InvoiceModels");
            DropIndex("dbo.InvoicePurchaseModels", new[] { "Invoice_Id" });
            DropIndex("dbo.InvoicePurchaseModels", new[] { "ProfileId" });
            DropTable("dbo.InvoicePurchaseModels");
        }
    }
}

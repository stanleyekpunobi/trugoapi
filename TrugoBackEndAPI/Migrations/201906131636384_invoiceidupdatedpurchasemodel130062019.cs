namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoiceidupdatedpurchasemodel130062019 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InvoicePurchaseModels", "Invoice_Id", "dbo.InvoiceModels");
            DropIndex("dbo.InvoicePurchaseModels", new[] { "Invoice_Id" });
            DropColumn("dbo.InvoicePurchaseModels", "InvoiceId");
            RenameColumn(table: "dbo.InvoicePurchaseModels", name: "Invoice_Id", newName: "InvoiceId");
            AlterColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.Int(nullable: false));
            AlterColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.Int(nullable: false));
            CreateIndex("dbo.InvoicePurchaseModels", "InvoiceId");
            AddForeignKey("dbo.InvoicePurchaseModels", "InvoiceId", "dbo.InvoiceModels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoicePurchaseModels", "InvoiceId", "dbo.InvoiceModels");
            DropIndex("dbo.InvoicePurchaseModels", new[] { "InvoiceId" });
            AlterColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.Int());
            AlterColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.String(nullable: false));
            RenameColumn(table: "dbo.InvoicePurchaseModels", name: "InvoiceId", newName: "Invoice_Id");
            AddColumn("dbo.InvoicePurchaseModels", "InvoiceId", c => c.String(nullable: false));
            CreateIndex("dbo.InvoicePurchaseModels", "Invoice_Id");
            AddForeignKey("dbo.InvoicePurchaseModels", "Invoice_Id", "dbo.InvoiceModels", "Id");
        }
    }
}

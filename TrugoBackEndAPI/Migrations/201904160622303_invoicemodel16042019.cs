namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicemodel16042019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "InvoiceGuid", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceModels", "InvoiceGuid");
        }
    }
}

namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoiemodel15042019 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvoiceModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceNumber = c.String(nullable: false),
                        InvoiceTitle = c.String(nullable: false),
                        Client = c.String(nullable: false),
                        InvoiceDate = c.DateTime(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        Amount = c.Int(nullable: false),
                        DiscountRate = c.Int(nullable: false),
                        SalePrice = c.Int(nullable: false),
                        Comment = c.String(),
                        Invitations = c.String(),
                        ProfileId = c.String(maxLength: 128),
                        InvoicePath = c.String(nullable: false),
                        Date_Created = c.DateTime(nullable: false),
                        AdminStatus = c.String(),
                        UserStatus = c.String(),
                        IsFeatured = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountSetupModels", t => t.ProfileId)
                .Index(t => t.ProfileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoiceModels", "ProfileId", "dbo.AccountSetupModels");
            DropIndex("dbo.InvoiceModels", new[] { "ProfileId" });
            DropTable("dbo.InvoiceModels");
        }
    }
}

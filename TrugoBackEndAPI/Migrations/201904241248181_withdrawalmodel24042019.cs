namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class withdrawalmodel24042019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WithdrawCreditModels", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WithdrawCreditModels", "Status");
        }
    }
}

namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicepayments05062019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoicePurchaseModels", "Date_Created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoicePurchaseModels", "Date_Created");
        }
    }
}

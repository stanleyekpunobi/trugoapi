// <auto-generated />
namespace TrugoBackEndAPI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class withdrawalmodel24042019 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(withdrawalmodel24042019));
        
        string IMigrationMetadata.Id
        {
            get { return "201904241248181_withdrawalmodel24042019"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class verificationstatus23042019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CreditRequestModels", "Verified", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccountVerified", c => c.Boolean(nullable: false));
            AddColumn("dbo.IncomeStatementModels", "Balance", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceModels", "Verified", c => c.Boolean(nullable: false));
            AddColumn("dbo.WithdrawCreditModels", "Verified", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WithdrawCreditModels", "Verified");
            DropColumn("dbo.InvoiceModels", "Verified");
            DropColumn("dbo.IncomeStatementModels", "Balance");
            DropColumn("dbo.AspNetUsers", "AccountVerified");
            DropColumn("dbo.CreditRequestModels", "Verified");
        }
    }
}

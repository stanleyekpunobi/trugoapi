namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class withdraw_creditModel15042019 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WithdrawCreditModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        Date_Estimate = c.DateTime(nullable: false),
                        Date_Created = c.DateTime(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountSetupModels", t => t.ProfileId)
                .Index(t => t.ProfileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WithdrawCreditModels", "ProfileId", "dbo.AccountSetupModels");
            DropIndex("dbo.WithdrawCreditModels", new[] { "ProfileId" });
            DropTable("dbo.WithdrawCreditModels");
        }
    }
}

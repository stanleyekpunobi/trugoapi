namespace TrugoBackEndAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class codechangedtoguidpurchasemodel130062019 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InvoicePurchaseModels", "Code", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvoicePurchaseModels", "Code", c => c.String(nullable: false));
        }
    }
}

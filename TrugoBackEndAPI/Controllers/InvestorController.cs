﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DalClasses;
using TrugoBackEndAPI.Models.DatabaseModelss;

namespace TrugoBackEndAPI.Controllers
{
    [RoutePrefix("api/Investor")]
    [AllowAnonymous]
    public class InvestorController : ApiController
    {

        private InvestorDal _investorDal;
        private ApiHelperClass _apiHelperClass;

        public InvestorController()
        {

        }

        public InvestorController(InvestorDal sellerDal, ApiHelperClass apiHelperClass)
        {
            InvestorDal = _investorDal;
            ApiHelperClass = apiHelperClass;
        }

        public InvestorDal InvestorDal
        {
            get
            {
                return new InvestorDal();
            }
            private set
            {
                _investorDal = value;
            }
        }

        public ApiHelperClass ApiHelperClass
        {
            get
            {
                return new ApiHelperClass();
            }
            private set
            {
                _apiHelperClass = value;
            }
        }
        [HttpGet]
        [Route("GetDashboardData")]
        public IHttpActionResult GetDashboardData(string userid)
        {
      
            var investorDashboardData = InvestorDal.GetDashboardData(userid);

            return Ok(investorDashboardData);
        }

        [HttpGet]
        [Route("GetDashboarChartdData")]
        public IHttpActionResult GetDashboardChartData(string userid)
        {
           
            var investorDashboardData = InvestorDal.GetDashboardChartData(userid);

            return Ok(investorDashboardData);
        }

        [HttpPost]
        [Route("PurchaseInvoice")]
        public async Task<IHttpActionResult> PurchaseInvoiceAsync(InvoicePurchaseModel model)
        {
            IHttpActionResult result = BadRequest("an error occured");

            if (!ModelState.IsValid)
            {
                result = BadRequest("invalid invoice purchase details");
            }
            else
            {
                var purchaseStatus = await InvestorDal.PurchaseInvoice(model);

                if (purchaseStatus)
                {
                    var responseDictionary = new Dictionary<string, bool>()
                    {
                        {"status", purchaseStatus }
                    };

                    result = Ok(responseDictionary);
                }

            }

            return result;
        }
    }
}

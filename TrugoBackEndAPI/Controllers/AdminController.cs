﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DalClasses;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Controllers
{
    [RoutePrefix("api/Admin")]
    [Authorize(Roles = access)]
    [AllowAnonymous]
    public class AdminController : ApiController
    {
        private const string access = "admin,approver,verifier";

        #region dependencies
        private ApiHelperClass _apiHelperClass;

        private AdminDal _adminDal;

        private CreditDal _creditDal;

        private InvoiceDal _invoiceDal;

        private NotificationHelperClass _notificationHelperClass;

        private ApplicationUserManager _userManager;

        private ApplicationRoleManager _roleManager;

        public AdminController()
        {

        }

        public ApiHelperClass ApiHelperClass
        {
            get
            {
                return new ApiHelperClass();
            }

            set
            {
                _apiHelperClass = value;
            }
        }

        public InvoiceDal InvoiceDal
        {
            get
            {
                return new InvoiceDal();
            }

            set
            {
                _invoiceDal = value;
            }
        }

        public NotificationHelperClass NotificationHelperClass
        {
            get
            {
                return new NotificationHelperClass();
            }

            set
            {
                _notificationHelperClass = value;
            }
        }

        public AdminDal AdminDal
        {
            get
            {
                return new AdminDal(NotificationHelperClass);
            }

            set
            {
                _adminDal = value;
            }
        }

        public CreditDal CreditDal
        {
            get
            {
                return new CreditDal();
            }

            set
            {
                _creditDal = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public AdminController(ApiHelperClass apiHelperClass, AdminDal adminDal,
         NotificationHelperClass notificationHelperClass, ApplicationUserManager userManager,
         ApplicationRoleManager roleManager, CreditDal creditDal, InvoiceDal invoiceDal)
        {
            ApiHelperClass = apiHelperClass;
            AdminDal = adminDal;
            NotificationHelperClass = notificationHelperClass;
            UserManager = userManager;
            RoleManager = roleManager;
            CreditDal = creditDal;
            InvoiceDal = invoiceDal;
        }

        #endregion

        #region user operations
        [HttpGet]
        [Route("GetAllUsers")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetAllUsersAsync()
        {
            var users = AdminDal.GetAllUsers();

            if (users.Count() <= 0)
            {
                return NotFound();
            }
            else
            {
                var meta = new MetaData(1, 1, -1, users.Count(), "desc", "Date_Created");

                foreach (var item in users)
                {
                    var accountType = await UserManager.GetRolesAsync(item.UserId);
                    item.AccountType = accountType.FirstOrDefault();
                }

                var userResponseModel = new UsersResponseModel()
                {
                    meta = meta,
                    data = users,
                };

                return Ok(userResponseModel);
            }
        }

        [HttpGet]
        [Route("GetPendingUsers")]
        public async Task<IHttpActionResult> GetPendingUsers()
        {
            var users = AdminDal.GetPendingUsers();

            if (users.Count() <= 0)
            {
                return NotFound();
            }
            else
            {
                var meta = new MetaData(1, 1, -1, users.Count(), "desc", "Date_Created");

                foreach (var item in users)
                {
                    var accountType = await UserManager.GetRolesAsync(item.UserId);
                    item.AccountType = accountType.FirstOrDefault();
                }

                var userResponseModel = new UsersResponseModel()
                {
                    meta = meta,
                    data = users,
                };

                return Ok(userResponseModel);
            }
        }

        [HttpGet]
        [Route("GetVerifiedUsers")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetVerifiedUsers()
        {
            var users = AdminDal.GetVerifiedUsers();

            if (users.Count() <= 0)
            {
                return NotFound();
            }
            else
            {
                var meta = new MetaData(1, 1, -1, users.Count(), "desc", "Date_Created");

                foreach (var item in users)
                {
                    var accountType = await UserManager.GetRolesAsync(item.UserId);
                    item.AccountType = accountType.FirstOrDefault();
                }

                var userResponseModel = new UsersResponseModel()
                {
                    meta = meta,
                    data = users,
                };

                return Ok(userResponseModel);
            }
        }

        [HttpGet]
        [Route("GetApprovedUsers")]
        [Authorize(Roles = "admin,approver")]
        public async Task<IHttpActionResult> GetApprovedUsers()
        {
            var users = AdminDal.GetApprovedUsers();

            if (users.Count() <= 0)
            {
                return NotFound();
            }
            else
            {
                var meta = new MetaData(1, 1, -1, users.Count(), "desc", "Date_Created");

                foreach (var item in users)
                {
                    var accountType = await UserManager.GetRolesAsync(item.UserId);
                    item.AccountType = accountType.FirstOrDefault();
                }

                var userResponseModel = new UsersResponseModel()
                {
                    meta = meta,
                    data = users,
                };

                return Ok(userResponseModel);
            }
        }

        [HttpPost]
        [Route("ApproveUserProfile")]
        [Authorize(Roles = "admin,approver")]
        public IHttpActionResult ApproveUserprofile(UserRequestModel model)
        {
            var status = AdminDal.ApproveUserProfile(model.UserId);
            var responseObject = new Dictionary<string, bool>();
            responseObject.Add("Status", status);
            return Ok(responseObject);
        }


        [HttpPost]
        [Route("VerifyUserProfile")]
        [Authorize(Roles = access)]
        [AllowAnonymous]
        public IHttpActionResult VerifyUserprofile(UserRequestModel model)
        {
            var status = AdminDal.VerifyUserProfile(model.UserId);
            var responseObject = new Dictionary<string, bool>();
            responseObject.Add("Status", status);
            return Ok(responseObject);
        }

        [HttpPost]
        [Route("AddAdmin")]
        // [Authorize(Roles = "admin")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AddAdminUser(AddAdminRequestLibraryModel model)
        {
            IHttpActionResult result = BadRequest();
            try
            {
                var user = await UserManager.FindByEmailAsync(model.UserId);

                if (user != null)
                {
                    var role = RoleManager.FindByName(model.Role);
                    if (role != null)
                    {
                        var status = UserManager.AddToRole(user.Id, model.Role);

                        var responseObject = new Dictionary<string, bool>();

                        if (status.Succeeded)
                        {
                            responseObject.Add("Status", status.Succeeded);
                            result = Ok(responseObject);
                        }
                        else
                        {
                            responseObject.Add("Status", status.Succeeded);
                            result = BadRequest("False");
                        }
                    }

                }
                else
                {
                    result = BadRequest("False");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        [HttpPost]
        [Route("RevokeUserProfile")]
        [AllowAnonymous]
        public IHttpActionResult RevokeUserProfile(UserRequestModel model)
        {
            IHttpActionResult result = BadRequest();

            if (string.IsNullOrEmpty(model.UserId))
            {
                return BadRequest("invalid user");
            }
            else
            {
                var responseDictionary = new Dictionary<string, bool>();

                var revokeStatus = AdminDal.RevokeUserVerification(model.UserId);

                if (revokeStatus)
                {
                    responseDictionary.Add("status", revokeStatus);
                    result = Ok(responseDictionary);
                }
                else
                {
                    result = BadRequest();
                }

            }

            return result;
        }

        [HttpPost]
        [Route("DeclineUserProfile")]
        [Authorize(Roles = "admin,approver")]
        public IHttpActionResult DeclineUserProfile(UserRequestModel model)
        {
            var status = AdminDal.DeclineUserProfile(model.UserId, model.Message);
            var responseObject = new Dictionary<string, bool>();
            responseObject.Add("Status", status);
            return Ok(responseObject);
        }

        [HttpGet]
        [Route("GetUserProfile")]
        public IHttpActionResult GetUserProfile(string userId)
        {
            return null;
        }

        #endregion user operations

        #region credit requests
        [HttpGet]
        [Route("GetCreditRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetCreditRequests()
        {
            var creditRequests = CreditDal.GetAllCreditRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetPendingCreditRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetPendingCreditRequests()
        {
            var creditRequests = CreditDal.GetPendingCreditRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetVerifiedCreditRequest")]
        [AllowAnonymous]
        public IHttpActionResult GetVerifiedCreditRequests()
        {
            var creditRequests = CreditDal.GetVerifiedCreditRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetApprovedCreditRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetApprovedCreditRequests()
        {
            var creditRequests = CreditDal.GetApprovedCreditRequests();

            return Ok(creditRequests);
        }

        [HttpPost]
        [Route("GetCreditRequest")]
        [AllowAnonymous]
        public IHttpActionResult GetCreditRequest(CreditLibraryRequestModel model)
        {
            var creditRequest = CreditDal.GetCreditRequest(model);

            if (!string.IsNullOrEmpty(creditRequest.Profile.UserId))
            {
                return Ok(creditRequest);

            }
            else
            {
                return BadRequest("credit request not found");
            }
        }

        [HttpPost]
        [Route("ApproveCreditRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ApproveCreditRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.ApproveCreditRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("VerifyCreditRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> VerifyCreditRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.VerifyCreditRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("DeclineCreditRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeclineCreditRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.DeclineCreditRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("RevokeVerifiedCreditRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> RevokeVerifiedCreditRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.RevokeVerifiedCreditRequest(model);

            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);
            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        #endregion credit requests

        #region withdrawal requests
        [HttpGet]
        [Route("GetWithdrawalRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetWithdrawalRequests()
        {
            var creditRequests = CreditDal.GetAllWithdrawalRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetPendingWithdrawalRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetPendingWithdrawalRequests()
        {
            var creditRequests = CreditDal.GetPendingWithdrawalRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetApprovedWithdrawalRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetApprovedWithdrawalRequests()
        {
            var creditRequests = CreditDal.GetApprovedWithdrawalRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetVerifiedWithdrawalRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetVerifiedWithdrawalRequests()
        {
            var creditRequests = CreditDal.GetVerifiedWithdrawalRequests();

            return Ok(creditRequests);
        }

        [HttpPost]
        [Route("GetWithdrawalRequest")]
        [AllowAnonymous]
        public IHttpActionResult GetWithdrawalRequest(CreditLibraryRequestModel model)
        {
            var creditRequest = CreditDal.GetWithdrawalRequest(model);
            if (!string.IsNullOrEmpty(creditRequest.Profile.UserId))
            {
                return Ok(creditRequest);

            }
            else
            {
                return BadRequest("credit request not found");
            }
        }

        [HttpPost]
        [Route("ApproveWithdrawalRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ApproveWithdrawalRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.ApproveWithdrawalRequest(model);

            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);
            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("VerifyWithdrawalRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> VerifyWithdrawalRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.VerifyWithdrawalRequest(model);

            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);
            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("DeclineWithdrawalRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeclineWithdrawalRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.DeclineWithdrawalRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("RevokeVerifiedRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> RevokeVerifiedRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.RevokeVerifiedWithdrawalRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);
            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        #endregion withdrawal requests

        #region invoice operations

        [Route("GetAllInvoices")]
        [HttpGet]
        public IHttpActionResult GetAllInvoices()
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var invoices = new List<InvoiceResponseModel>();

            try
            {
                invoices = InvoiceDal.GetAllInvoices();
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoices);
        }

        [Route("VerifyInvoice")]
        public async Task<IHttpActionResult> VerifyInvoiceAsync(InvoiceRequestModel request)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            IHttpActionResult result = null;

            bool isSuccessful = false;

            try
            {
                isSuccessful = await InvoiceDal.VerifyInvoiceAsync(request);

                if (isSuccessful == true)
                {
                    responseObject.Add("Status", true);
                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        [Route("ApproveInvoice")]
        public async Task<IHttpActionResult> ApproveInvoiceAsync(InvoiceRequestModel request)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            IHttpActionResult result = null;

            bool isSuccessful = false;

            try
            {
                isSuccessful = await InvoiceDal.ApproveInvoiceAsync(request);

                if (isSuccessful == true)
                {
                    responseObject.Add("Status", true);
                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        [Route("RevokeInvoiceVerification")]
        public async Task<IHttpActionResult> RevokeInvoiceVerificationAsync(InvoiceRequestModel request)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            IHttpActionResult result = null;

            bool isSuccessful = false;

            try
            {
                isSuccessful = await InvoiceDal.RevokeInvoiceVerification(request);

                if (isSuccessful == true)
                {
                    responseObject.Add("Status", true);
                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        [Route("ModerateInvoice")]
        public async Task<IHttpActionResult> ModerateInvoiceAsync(InvoiceRequestModel request)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            IHttpActionResult result = null;

            bool isSuccessful = false;

            try
            {
                isSuccessful = await InvoiceDal.ModerateInvoice(request);

                if (isSuccessful == true)
                {
                    responseObject.Add("Status", true);
                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        [Route("DeclineInvoice")]
        public async Task<IHttpActionResult> DeclineInvoiceAsync(InvoiceRequestModel request)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            IHttpActionResult result = null;

            bool isSuccessful = false;

            try
            {
                isSuccessful = await InvoiceDal.DeclineInvoice(request);

                if (isSuccessful == true)
                {
                    responseObject.Add("Status", true);
                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        #region credit requests
        [HttpGet]
        [Route("GetInvoiceRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetInvoiceRequests()
        {
            var creditRequests = CreditDal.GetAllCreditRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetPendingInvoiceRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetPendingInvoiceRequests()
        {
            var creditRequests = CreditDal.GetPendingCreditRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetVerifiedInvoiceRequest")]
        [AllowAnonymous]
        public IHttpActionResult GetVerifiedInvoiceRequests()
        {
            var creditRequests = CreditDal.GetVerifiedCreditRequests();

            return Ok(creditRequests);
        }

        [HttpGet]
        [Route("GetApprovedInvoiceRequests")]
        [AllowAnonymous]
        public IHttpActionResult GetApprovedInvoiceRequests()
        {
            var creditRequests = CreditDal.GetApprovedCreditRequests();

            return Ok(creditRequests);
        }

        [HttpPost]
        [Route("GetInvoiceRequest")]
        [AllowAnonymous]
        public IHttpActionResult GetInvoiceRequest(CreditLibraryRequestModel model)
        {
            var creditRequest = CreditDal.GetCreditRequest(model);

            if (!string.IsNullOrEmpty(creditRequest.Profile.UserId))
            {
                return Ok(creditRequest);

            }
            else
            {
                return BadRequest("credit request not found");
            }
        }

        [HttpPost]
        [Route("ApproveInvoiceRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ApproveInvoiceRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.ApproveCreditRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("VerifyInvoiceRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> VerifyInvoiceRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.VerifyCreditRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("DeclineInvoiceRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> DeclineInvoiceRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.DeclineCreditRequest(model);
            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);

            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        [HttpPost]
        [Route("RevokeVerifiedInvoiceRequest")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> RevokeVerifiedInvoiceRequestAsync(CreditLibraryRequestModel model)
        {
            var approvalStatus = await CreditDal.RevokeVerifiedCreditRequest(model);

            if (approvalStatus)
            {
                var responseObject = new Dictionary<string, bool>();
                responseObject.Add("Status", approvalStatus);
                return Ok(responseObject);
            }
            else
            {
                return BadRequest("an error has occured");
            }
        }

        #endregion credit requests
        #endregion
    }
}

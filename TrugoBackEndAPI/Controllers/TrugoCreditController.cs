﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using TrugoBackEndAPI.Models.DalClasses;
using TrugoBackEndAPI.Models.DatabaseModelss;

namespace TrugoBackEndAPI.Controllers
{
    [RoutePrefix("api/TrugoCredit")]
    public class TrugoCreditController : ApiController
    {
        private CreditDal _creditDal;

        private UserAccountDal _userAccountDal;

        public TrugoCreditController()
        {

        }

        public TrugoCreditController(CreditDal creditDal, UserAccountDal userAccountDal)
        {
            CreditDal = creditDal;
            UserAccountDal = userAccountDal;
        }

        public CreditDal CreditDal
        {
            get
            {
                return new CreditDal();
            }
            private set
            {
                _creditDal = value;
            }
        }

        public UserAccountDal UserAccountDal
        {
            get
            {
                return new UserAccountDal();
            }
            private set
            {
                _userAccountDal = value;
            }
        }

        [Route("RequestCredit")]
        public async Task<IHttpActionResult> RequestCreditAsync(CreditRequestModel model)
        {
            IHttpActionResult result = null;

            var responseObject = new Dictionary<string, bool>();

            if (ModelState.IsValid)
            {

                var userProfileStatus = UserAccountDal.CheckProfile(model.ProfileId);

                if (!userProfileStatus)
                {
                    return result = BadRequest("no user profile found");
                }
                   
                var isSuccessful = await CreditDal.CreateCreditRequest(model);

                if (!isSuccessful)
                {
                    result = BadRequest();
                }
                else
                {
                    responseObject.Add("status", true);
                    return Ok(responseObject);
                }
            }
            else
            {
                result = BadRequest(ModelState);
            }

            return result;
        }

        [Route("WithdrawCredit")]
        public async Task<IHttpActionResult> WithdrawCreditAsync(WithdrawCreditModel model)
        {
            IHttpActionResult result = null;

            var responseObject = new Dictionary<string, bool>();

            if (ModelState.IsValid)
            {
                var userProfileStatus = UserAccountDal.CheckProfile(model.ProfileId);

                if (!userProfileStatus)
                {
                    return result = BadRequest("no user profile found");
                }

                var isSuccessful = await CreditDal.CreateWithdrawalRequest(model);

                if (!isSuccessful)
                {
                    result = BadRequest();
                }
                else
                {
                    responseObject.Add("status", true);
                    return Ok(responseObject);
                }
            }
            else
            {
                result = BadRequest(ModelState);
            }
            return result;
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}

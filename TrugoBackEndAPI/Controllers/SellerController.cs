﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DalClasses;
using TrugoBackEndAPI.Models.DatabaseModelss;

namespace TrugoBackEndAPI.Controllers
{
    [RoutePrefix("api/Seller")]
    public class SellerController : ApiController
    {
        private SellerDal _sellerDal;
        private ApiHelperClass _apiHelperClass;

        public SellerController()
        {

        }

        public SellerController(SellerDal sellerDal, ApiHelperClass apiHelperClass)
        {
            SellerDal = sellerDal;
            ApiHelperClass = apiHelperClass;
        }

        public SellerDal SellerDal
        {
            get
            {
                return new SellerDal();
            }
            private set
            {
                _sellerDal = value;
            }
        }

        public ApiHelperClass ApiHelperClass
        {
            get
            {
                return new ApiHelperClass();
            }
            private set
            {
                _apiHelperClass = value;
            }
        }

        [HttpGet]
        [Route("GetDashboardData")]
        [AllowAnonymous]
        public IHttpActionResult GetDashboardData(string userid)
        {
            var dashboardData = SellerDal.GetSellerDashboardData(userid);

            return Ok(dashboardData);
        }


        [HttpPost]
        [Route("PostInvoice")]
        public async Task<IHttpActionResult> PostInvoiceAsync()
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            IHttpActionResult result = null;

            var responseObject = new Dictionary<string, bool>();

            try
            {
                var invoiceModel = new InvoiceModel();

                invoiceModel.InvoiceTitle = HttpContext.Current.Request.Params["invoicetitle"];

                invoiceModel.Client = HttpContext.Current.Request.Params["client"];

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["invoicedate"]))
                {
                    invoiceModel.InvoiceDate = Convert.ToDateTime(HttpContext.Current.Request.Params["invoicedate"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["duedate"]))
                {
                    invoiceModel.DueDate = Convert.ToDateTime(HttpContext.Current.Request.Params["duedate"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["amount"]))
                {
                    invoiceModel.Amount = Convert.ToInt32(HttpContext.Current.Request.Params["amount"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["discountrate"]))
                {
                    invoiceModel.DiscountRate = Convert.ToInt32(HttpContext.Current.Request.Params["discountrate"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["saleprice"]))
                {
                    invoiceModel.SalePrice = Convert.ToInt32(HttpContext.Current.Request.Params["saleprice"]);
                }

                invoiceModel.Comment = HttpContext.Current.Request.Params["comment"];

                invoiceModel.Invitations = HttpContext.Current.Request.Params["invitations"];

                invoiceModel.ProfileId = HttpContext.Current.Request.Params["profileid"];

                invoiceModel.InvoiceNumber = SellerDal.GenerateInvoiceNumber(invoiceModel.ProfileId);

                invoiceModel.Date_Created = DateTime.Now;

                invoiceModel.AdminStatus = "not approved";

                invoiceModel.UserStatus = "on sale";

                invoiceModel.InvoiceGuid = Guid.NewGuid();

                var httpRequest = HttpContext.Current.Request;

                var getfiles = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;

                foreach (string item in getfiles)
                {
                    var file = httpRequest.Files[item];
                    string filesavePath = string.Empty;
                    if (file != null && file.ContentLength > 0)
                    {
                        if (item == "invoicefile")
                        {
                            filesavePath = @"~/App_Data/Invoices/" + invoiceModel.ProfileId;

                            invoiceModel.InvoicePath = $"{invoiceModel.ProfileId}{file.FileName}";

                            var path = Path.Combine(HttpContext.Current.Server.MapPath(filesavePath), $"{invoiceModel.InvoiceNumber}{invoiceModel.ProfileId}{file.FileName}");

                            var path2 = Path.Combine(HttpContext.Current.Server.MapPath(filesavePath));

                            if (Directory.Exists(path2))
                            {

                            }
                            else
                            {
                                Directory.CreateDirectory(path2);
                            }

                            file.SaveAs(path);
                        }
                        else { }
                      
                    }

                }

                var createInvoice = await SellerDal.CreateInvoice(invoiceModel);

                var emailurl = ApiHelperClass.GetReturnEmailHeaderValue(Request);

                if (!string.IsNullOrEmpty(emailurl) && !string.IsNullOrEmpty(invoiceModel.Invitations))
                {
                    var url = $"{emailurl}?invoiceid={invoiceModel.Id}&code={invoiceModel.InvoiceGuid}";

                    url = HttpUtility.UrlEncode(url);

                    var isSent = await SellerDal.SendInvoiceInvitations(url, invoiceModel.Invitations);
                }

                if (createInvoice == true)
                {
                    responseObject.Add("status", createInvoice);

                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        [HttpPost]
        [Route("UpdateInvoice")]
        public async Task<IHttpActionResult> UpdateInvoice()
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            IHttpActionResult result = null;

            var responseObject = new Dictionary<string, bool>();

            try
            {
                var invoiceModel = new InvoiceModel();

                invoiceModel.InvoiceTitle = HttpContext.Current.Request.Params["invoicetitle"];

                invoiceModel.Client = HttpContext.Current.Request.Params["client"];

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["invoicedate"]))
                {
                    invoiceModel.InvoiceDate = Convert.ToDateTime(HttpContext.Current.Request.Params["invoicedate"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["duedate"]))
                {
                    invoiceModel.DueDate = Convert.ToDateTime(HttpContext.Current.Request.Params["duedate"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["amount"]))
                {
                    invoiceModel.Amount = Convert.ToInt32(HttpContext.Current.Request.Params["amount"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["discountrate"]))
                {
                    invoiceModel.DiscountRate = Convert.ToInt32(HttpContext.Current.Request.Params["discountrate"]);
                }

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Params["saleprice"]))
                {
                    invoiceModel.SalePrice = Convert.ToInt32(HttpContext.Current.Request.Params["saleprice"]);
                }

                invoiceModel.Comment = HttpContext.Current.Request.Params["comment"];

                invoiceModel.Invitations = HttpContext.Current.Request.Params["invitations"];

                invoiceModel.ProfileId = HttpContext.Current.Request.Params["profileid"];

                invoiceModel.InvoiceNumber = SellerDal.GenerateInvoiceNumber(invoiceModel.ProfileId);

                var httpRequest = HttpContext.Current.Request;

                var getfiles = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;

                foreach (string item in getfiles)
                {
                    var file = httpRequest.Files[item];
                    string filesavePath = string.Empty;
                    if (file != null && file.ContentLength > 0)
                    {
                        if (item == "invoicefile")
                        {
                            filesavePath = @"~/App_Data/Invoices/" + invoiceModel.ProfileId;

                            invoiceModel.InvoicePath = $"{invoiceModel.ProfileId}{file.FileName}";

                            var path = Path.Combine(HttpContext.Current.Server.MapPath(filesavePath), $"{invoiceModel.InvoiceNumber}{invoiceModel.ProfileId}{file.FileName}");

                            var path2 = Path.Combine(HttpContext.Current.Server.MapPath(filesavePath));

                            if (Directory.Exists(path2))
                            {

                            }
                            else
                            {
                                Directory.CreateDirectory(path2);
                            }

                            file.SaveAs(path);
                        }
                        else { }

                    }

                }

                var updateInvoice = await SellerDal.Updateinvoice(invoiceModel);

                var emailurl = ApiHelperClass.GetReturnEmailHeaderValue(Request);

                if (!string.IsNullOrEmpty(emailurl) && !string.IsNullOrEmpty(invoiceModel.Invitations))
                {
                    var url = $"{emailurl}?token={invoiceModel.InvoiceGuid}&invoice={invoiceModel.InvoiceNumber}&profileid={invoiceModel.ProfileId}";

                    url = HttpUtility.UrlEncode(url);

                    var isSent = await SellerDal.SendInvoiceInvitations(url, invoiceModel.Invitations);
                }

                //if (createInvoice == true)
                //{
                //    responseObject.Add("status", createInvoice);

                //    result = Ok(responseObject);
                //}
                //else
                //{
                //    result = BadRequest();
                //}
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }


        [HttpGet]
        [Route("GetDashboarChartdData")]
        [AllowAnonymous]
        public IHttpActionResult GetDashboarChartdData(string userid)
        {
            var dashboardData = SellerDal.GetDashboardChartData(userid);

            return Ok(dashboardData);
        }


    }
}

﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TrugoBackEndAPI.Models;
using TrugoBackEndAPI.Models.ApiUtils;
using TrugoBackEndAPI.Models.DalClasses;
using TrugoBackEndAPI.Providers;
using TrugoBackEndAPI.Results;
using TruGoClassLibrary.Classes;

namespace TrugoBackEndAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    [HostAuthentication(DefaultAuthenticationTypes.ApplicationCookie)]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";

        private ApplicationUserManager _userManager;

        private ApplicationRoleManager _roleManager;

        private UserAccountDal _userAccountDal;

        private ApiHelperClass _apiHelper;

        private TokenClass _myToken;

        private string ApiUri = EnvironmentClass.ApiBaseUrl;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ISecureDataFormat<AuthenticationTicket> accessTokenFormat, ApplicationRoleManager roleManager, UserAccountDal userAccountDal)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            UserAccountDal = userAccountDal;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ApiHelperClass ApiHelperClass
        {
            get
            {
                return new ApiHelperClass();
            }
            private set
            {
                _apiHelper = value;
            }
        }

        public UserAccountDal UserAccountDal
        {
            get
            {
                return new UserAccountDal();
            }
            private set
            {
                _userAccountDal = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/InitiatePasswordChange
        [HttpPost]
        [Route("InitiatePasswordChange")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> InitiatePasswordChangeAsync(InitiatePasswordChangeClass model)
        {
            string callback = string.Empty;

            var responseObject = new Dictionary<string, bool>();

            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            try
            {
                var user = await UserManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var header = Request.Headers;

                    if (header.Contains("emailurl"))
                    {
                        callback = header.GetValues("emailurl").First();

                        if (string.IsNullOrEmpty(callback))
                        {
                            return BadRequest();
                        }
                        else
                        {
                            string str = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                            str = HttpUtility.UrlEncode(str);

                            var uri = $"{callback}?userid={user.Id}&code={str}";

                            var passwordResetModel = new PasswordResetClass()
                            {
                                CallbackUrl = uri,
                                Email = user.Email,
                            };

                            var emailsent = ApiHelperClass.SendPasswordResetEmail(passwordResetModel);

                            if (emailsent == true)
                            {
                                responseObject.Add("status", true);

                                return Ok(responseObject);
                            }
                            else
                            {
                                return BadRequest("Invalid email");
                            }
                        }
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(responseObject);
        }

        // POST api/Account/ResetPassword
        [Route("ResetPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResetPasswordAsync(ChangePasswordBindingModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var result = await UserManager.ResetPasswordAsync(model.UserId, model.Code, model.NewPassword);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            responseObject.Add("status", true);

            return Ok(responseObject);
        }

        //validate password reset token
        [HttpPost]
        [Route("ValidatePasswordResetToken")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ValidatePasswordResetTokenAsync(VerifyEmailClass model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var responseObject = new Dictionary<string, bool>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var user = UserManager.FindById(model.UserId);

                if (!await UserManager.VerifyUserTokenAsync(model.UserId, "ResetPassword", model.Code))
                {
                    return BadRequest("invalid token");
                }
                else
                {
                    responseObject.Add("status", true);

                    return Ok(responseObject);
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(responseObject);
        }

        //Get api/Account/ConfirmEmail
        [HttpPost]
        [Route("ConfirmEmail")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ConfirmEmailAsync(VerifyEmailClass model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            IHttpActionResult result = null;

            var callback = string.Empty;

            bool flag;

            var responseObject = new Dictionary<string, bool>();

            try
            {
                flag = (string.IsNullOrEmpty(model.UserId) ? true : string.IsNullOrWhiteSpace(model.Code));

                if (!flag)
                {
                    var identityResult = await UserManager.ConfirmEmailAsync(model.UserId, model.Code);

                    if (!identityResult.Succeeded)
                    {
                        result = GetErrorResult(identityResult);
                    }
                    else
                    {
                        responseObject.Add("status", true);
                        result = Ok(responseObject);
                    }
                }
                else
                {
                    ModelState.AddModelError("message", "User Id and Code are required");
                    result = BadRequest(ModelState);
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //Get api/account/GetUserRole
        [HttpGet]
        [Route("GetUserRoles")]
        [AllowAnonymous]
        public IHttpActionResult GetUserRoles(string userid)
        {
            var roles = UserManager.GetRoles(userid);

            if (roles == null)
            {
                return Ok("no roles found");
            }
            else
            {
                return Ok(roles);
            }
        }

        //Post api/Account/resentActivationemail
        [HttpPost]
        [Route("ResendActivationEmail")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResendActivationEmailAsync(ResendActivationEmailClass model)
        {
            IHttpActionResult result = null;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1422749");

            var responseObject = new Dictionary<string, bool>();

            try
            {
                string callback = string.Empty;

                var user = await UserManager.FindByIdAsync(model.UserId);

                if (user != null)
                {

                    var header = Request.Headers;

                    if (header.Contains("emailurl"))
                    {
                        callback = header.GetValues("emailurl").First();

                        if (string.IsNullOrEmpty(callback))
                        {
                            result = BadRequest();
                        }
                        else
                        {
                            string str = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                            str = HttpUtility.UrlEncode(str);

                            var uri = $"{callback}?userid={user.Id}&code={str}";

                            ConfirmEmailClass confirmEmailClass = new ConfirmEmailClass()
                            {
                                CallbackUrl = uri,
                                Email = user.Email,
                            };

                            var emailsent = ApiHelperClass.SendConfirmationEmail(confirmEmailClass, uri);

                            if (emailsent == true)
                            {
                                responseObject.Add("status", true);
                                result = Ok(responseObject);
                            }
                            else
                            {
                                result = BadRequest("Invalid email");
                            }

                        }
                    }
                    else
                    {
                        result = BadRequest();
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null && ticket.Properties.ExpiresUtc.HasValue && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            string profileCreated = "false";

            if (user?.Profiles != null)
            {
                profileCreated = "true";
            }
            else
            {

            }

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                var roles = oAuthIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName, user.Id, roles.FirstOrDefault().Value, user.EmailConfirmed.ToString(), user.AccountApproved.ToString(), profileCreated);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            IHttpActionResult responseResult = BadRequest();

            var apiHelperClass = new ApiHelperClass();

            var result = new IdentityResult();

            var user = new ApplicationUser();

            var responseObject = new Dictionary<string, string>();

            try
            {

                if (!ModelState.IsValid)
                {
                    responseResult = BadRequest(ModelState);
                }
                else
                {
                    var emailurl = apiHelperClass.GetReturnEmailHeaderValue(Request);

                    if (string.IsNullOrEmpty(emailurl))
                    {
                        responseResult = BadRequest("invalid header value");

                    }
                    else
                    {
                        user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

                        result = await UserManager.CreateAsync(user, model.Password);

                        UserManager.AddToRole(user.Id, model.Role);
                    }

                    if (result.Succeeded)
                    {
                        var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                        code = HttpUtility.UrlEncode(code);

                        var uri = $"{emailurl}?userid={user.Id}&code={code}";

                        var confirmEmailModel = new ConfirmEmailClass()
                        {
                            CallbackUrl = uri,
                            Email = model.Email,
                        };

                        apiHelperClass.SendConfirmationEmail(confirmEmailModel, code);

                        var loginModel = new LoginClass()
                        {
                            Email = model.Email,
                            Password = model.Password
                        };

                        var login = await LoginUserAsync(loginModel);

                        responseResult = Ok(_myToken);

                    }
                    else
                    {

                    }


                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return responseResult;
        }

        //Call Api to login and generate accesstoken after registration
        private async Task<HttpResponseMessage> CallApiTaskAsync(string apiEndPoint, Dictionary<string, string> model = null)
        {
            var response = new HttpResponseMessage();

            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiUri);

                    client.DefaultRequestHeaders.Accept.Clear();

                    if (_myToken != null)
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _myToken.AccessToken);
                    }
                    else
                    {
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    }

                    response = await client.PostAsync(apiEndPoint, model != null ? new FormUrlEncodedContent(model) : null);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }
        
        //Post api/Account/LoginUser
        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public async Task<IHttpActionResult> LoginUserAsync(LoginClass model)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            IHttpActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid username or password");
                }
                else
                {
                    var getUser = UserManager.FindByEmail(model.Email);

                    var userProfileStatus = UserAccountDal.CheckProfile(getUser?.Id);

                    if (!userProfileStatus)
                    {
                        result = BadRequest("no user profile found");
                    }

                    var tokenClass = new Dictionary<string, string>
                    {
                        { "grant_type", "password"},
                        { "username", model.Email},
                        { "password", model.Password},
                    };

                    var response = await CallApiTaskAsync("/Token", tokenClass);

                    if (!response.IsSuccessStatusCode)
                    {
                        var errors = await response.Content.ReadAsStringAsync();
                        var errormodel = JsonConvert.DeserializeObject<ErrorClass>(errors);
                        return BadRequest(errormodel.ErrorDescription);
                    }

                    result = Ok(_myToken = response.Content.ReadAsAsync<TokenClass>(new[] { new JsonMediaTypeFormatter() }).Result);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //onboard user
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> OnboardUser()
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            IHttpActionResult result = null;

            var responseObject = new Dictionary<string, bool>();

            try
            {
                var accountSetUpModel = new AccountSetupModel();

                accountSetUpModel.LastName = HttpContext.Current.Request.Params["lastname"];
                accountSetUpModel.FirstName = HttpContext.Current.Request.Params["firstname"];
                accountSetUpModel.Id = HttpContext.Current.Request.Params["userid"];

                accountSetUpModel.MiddleName = HttpContext.Current.Request.Params["middlename"];
                accountSetUpModel.Gender = HttpContext.Current.Request.Params["gender"];
                accountSetUpModel.InvestorType = HttpContext.Current.Request.Params["businesstype"];
                accountSetUpModel.BusinessName = HttpContext.Current.Request.Params["businessname"];
                accountSetUpModel.PhoneNumber = HttpContext.Current.Request.Params["phonenumber"];
                accountSetUpModel.Address = HttpContext.Current.Request.Params["address"];
                accountSetUpModel.City = HttpContext.Current.Request.Params["city"];
                accountSetUpModel.Country = HttpContext.Current.Request.Params["country"];
                accountSetUpModel.Industry = HttpContext.Current.Request.Params["industry"];
                accountSetUpModel.PrincipalClient = HttpContext.Current.Request.Params["client"];
                accountSetUpModel.EstimatedAnnualRevenue = HttpContext.Current.Request.Params["annualrevenue"];
                if (string.IsNullOrEmpty(HttpContext.Current.Request.Params["businessspan"]))
                {

                }
                else
                {
                    accountSetUpModel.BusinessSpan = Convert.ToInt32(HttpContext.Current.Request.Params["businessspan"]);

                }
                accountSetUpModel.InvoiceRange = HttpContext.Current.Request.Params["invoicerange"];
                accountSetUpModel.HowDidYouHearAboutUs = HttpContext.Current.Request.Params["howdidyouhearaboutus"];
                accountSetUpModel.ReferralName = HttpContext.Current.Request.Params["referralname"];
                var httpRequest = HttpContext.Current.Request;
                var getfiles = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
                foreach (string item in getfiles)
                {
                    var file = httpRequest.Files[item];
                    string filesavePath = string.Empty;
                    if (file != null && file.ContentLength > 0)
                    {
                        if (item == "coi")
                        {
                            filesavePath = @"~/App_Data/UserProfile/COI";
                            accountSetUpModel.COI = $"{accountSetUpModel.Id}{file.FileName}";
                        }
                        else if (item == "pod")
                        {
                            filesavePath = @"~/App_Data/UserProfile/POD";
                            accountSetUpModel.POD = $"{accountSetUpModel.Id}{file.FileName}";


                        }
                        else if (item == "mou")
                        {
                            filesavePath = @"~/App_Data/UserProfile/POD";
                            accountSetUpModel.MOU = $"{accountSetUpModel.Id}{file.FileName}";


                        }
                        else if (item == "meansofid")
                        {
                            filesavePath = @"~/App_Data/UserProfile/MOI";
                            accountSetUpModel.MOI = $"{accountSetUpModel.Id}{file.FileName}";

                        }
                        else { }

                        //var fileName = Path.GetFileName(file.FileName);

                        var path = Path.Combine(HttpContext.Current.Server.MapPath(filesavePath), $"{accountSetUpModel.Id}{file.FileName}");

                        file.SaveAs(path);

                    }

                }

                var createprofile = await UserAccountDal.SetupUserProfile(accountSetUpModel);

                if (createprofile == true)
                {
                    responseObject.Add("status", createprofile);
                    result = Ok(responseObject);
                }
                else
                {
                    result = BadRequest();
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get user profile 
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetUserProfile(string profileid)
        {
            var userProfile = UserAccountDal.GetUserProfile(profileid);

            if (string.IsNullOrEmpty(userProfile.Id))
            {
                return BadRequest("invalid user");
            }
            else
            {
                return Ok(userProfile);
            }
        }

        // POST api/Account/RegisterExternal
        //[Authorize]
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal()
        {
          
            var info = await Authentication.GetExternalLoginInfoAsync();

            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = info.Email, Email = info.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        //dispose resources
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}

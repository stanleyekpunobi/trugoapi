﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TrugoBackEndAPI.Models;
using TruGoClassLibrary;
using TruGoClassLibrary.RequestModels;

namespace TrugoBackEndAPI.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Role")]
    public class RoleController : ApiController
    {
        private ApplicationRoleManager _roleManager;

        private ApplicationUserManager _userManager;

        public RoleController()
        {
        }

        public RoleController(ApplicationRoleManager roleManager, ApplicationUserManager userManager)
        {
            RoleManager = roleManager;
            UserManager = userManager;

        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().Get<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        [Route("CreateRole")]
        public async Task<IHttpActionResult> CreateRoleAsync(RoleRequestModel model)
        {
            var responseDictionary = new Dictionary<string, string>();

            try
            {
                var role = new ApplicationRole() { Name = model.Name };
                var isCreated = await RoleManager.CreateAsync(role);
                if (isCreated.Succeeded)
                {
                    responseDictionary.Add("iscreated", "true");
                }
                else
                {
                    responseDictionary.Add("iscreated", isCreated.Errors.ToString());

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(responseDictionary);
        }

        [HttpPost]
        [Route("AddUserToRole")]
        public async Task<IHttpActionResult> AddUserToRole(AdduserRoleRequestModel model)
        {
            var responseObject = new Dictionary<string, bool>();

            IHttpActionResult result = null;

            try
            {
                if (ModelState.IsValid)
                {
                    var addUser = await UserManager.AddToRoleAsync(model.UserId, model.Role);

                    if (addUser.Succeeded)
                    {
                        responseObject.Add("status", true);
                        result = Ok(responseObject);
                    }
                    else
                    {
                        result = InternalServerError();

                    }
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        //override rolemanager resource
        protected override void Dispose(bool disposing)
        {
            if (disposing && _roleManager != null)
            {
                _roleManager.Dispose();
                _roleManager = null;
            }

            base.Dispose(disposing);
        }
    }
}

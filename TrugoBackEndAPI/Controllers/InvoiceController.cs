﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TrugoBackEndAPI.Models.DalClasses;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoBackEndAPI.Controllers
{
    [RoutePrefix("api/Invoice")]
    [Authorize]
    public class InvoiceController : ApiController
    {
        private InvoiceDal _invoiceDal;

        public InvoiceDal InvoiceDal
        {
            get
            {
                return new InvoiceDal();
            }
            private set
            {
                _invoiceDal = value;
            }
        }

        public InvoiceController()
        {

        }

        public InvoiceController(InvoiceDal invoiceDal)
        {
            InvoiceDal = invoiceDal;
        }
     

        [AllowAnonymous]
        [Route("GetInvoice")]
        [HttpGet]
        public IHttpActionResult GetInvoice(string invoiceid, string code)
        {
            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var invoices = new List<InvoiceResponseModel>();

            try
            {
                invoices = InvoiceDal.GetAllInvoices();
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoices);
        }

        [AllowAnonymous]
        [Route("GetInvoiceDetail")]
        [HttpGet]
        public IHttpActionResult GetInvoiceDetail(int? invoiceid, string code)
        {


            var ravenClient = new RavenClient("https://9985494c8e6046c1b93e21f42477afb6@sentry.io/1422749");

            var invoices = new InvoiceDetailResponseModel();

            try
            {
                invoices = InvoiceDal.GetInvoiceDetail(invoiceid,code);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoices);
        }

        [AllowAnonymous]
        [Route("LoadInvoices")]
        public IHttpActionResult GetInvoices()
        {
            var invoices = InvoiceDal.GetAllInvoices();

            if (invoices != null)
            {
                return Ok(invoices);
            }
            else
            {
                return NotFound();
            }
            
        }
    }
}

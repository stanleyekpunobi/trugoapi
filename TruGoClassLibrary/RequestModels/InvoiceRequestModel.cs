﻿using System;

namespace TruGoClassLibrary.RequestModels
{
    public class InvoiceRequestModel
    {
        public int Invoiceid { get; set; }

        public Guid Code { get; set; }

       public string UserId { get; set; }

        public string Message { get; set; }
    }
}

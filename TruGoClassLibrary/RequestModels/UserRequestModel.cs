﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.RequestModels
{
    public class UserRequestModel
    {
        public string UserId { get; set; }
        public string Message { get; set; }
    }
}

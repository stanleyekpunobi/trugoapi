﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.RequestModels
{
    public class InvoiceSearchRequestModel
    {
        public int Invoicemin { get; set; }
        public int Invoicemax { get; set; }
        public int Discount { get; set; }

        public string Date { get; set; }

        public string SearchTerm { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.RequestModels
{
   
    public class CreditLibraryRequestModel
    {
        public int Id { get; set; }
        public string Userid { get; set; }
        public string Message { get; set; }
        public int CreditAmount { get; set; }
    }
}

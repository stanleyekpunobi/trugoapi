﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.Classes
{
  public  class TokenClass
    {
        [JsonProperty("access_token")]
        public string AccessToken
        {
            get;
            set;
        }

        [JsonProperty(".expires")]
        public DateTime Expired
        {
            get;
            set;
        }

        [JsonProperty("expires_in")]
        public int ExpiresIn
        {
            get;
            set;
        }

        [JsonProperty(".issued")]
        public DateTime Issued
        {
            get;
            set;
        }

        [JsonProperty("token_type")]
        public string TokenType
        {
            get;
            set;
        }

        [JsonProperty("userId")]
        public string UserId
        {
            get;
            set;
        }

        [JsonProperty("userName")]
        public string UserName
        {
            get;
            set;
        }

        [JsonProperty("emailconfirmed")]
        public string EmailConfirmed
        {
            get;
            set;
        }
        [JsonProperty("accountapproved")]
        public string AccountApproved
        {
            get;
            set;
        }
        [JsonProperty("accounttype")]
        public string role
        {
            get;
            set;
        }

        [JsonProperty("profilecreated")]
        public string profilecreated
        {
            get;
            set;
        }

    }
}

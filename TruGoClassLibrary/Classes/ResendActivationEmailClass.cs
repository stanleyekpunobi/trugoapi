﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.Classes
{
   public class ResendActivationEmailClass
    {
        [Required]
        public string UserId { get; set; }
    }
}

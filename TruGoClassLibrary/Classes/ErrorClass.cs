﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.Classes
{
    public class ErrorClass
    {
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
    }
}

﻿namespace TruGoClassLibrary.Classes
{
    public class PasswordResetClass
    {
        public string CallbackUrl { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
    }
}

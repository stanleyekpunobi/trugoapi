﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.Classes
{
    public class ConfirmEmailClass
    {
        public string CallbackUrl { get; set; }
        public string Email { get; set; }
    }
}

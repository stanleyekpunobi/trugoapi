﻿using System;
using System.Collections.Generic;

namespace TruGoClassLibrary.ResponseClass
{
    public class UsersResponseModel
    {
        public MetaData meta { get; set; }
        public List<UserprofileData> data { get; set; }
    }

    public class MetaData
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public int PerPage { get; set; }
        public int Total { get; set; }
        public string Sort { get; set; }
        public string Field { get; set; }

        public MetaData(int page, int pages, int perpage, int total, string sort, string field)
        {
            Page = page;
            Pages = pages;
            PerPage = perpage;
            Total = total;
            Sort = sort;
            Field = field;
           
        }

        //get page total for
        private double GetPageTotal(int total, int limit)
        {
            var result = total / (float)limit;
            return (int)Math.Ceiling(result);
        }

    }

    public class UserprofileData
    {
        public string UserId { get; set; }
        public DateTime Date_Created { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string InvestorType { get; set; }
        public string BusinessName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Industry { get; set; }
        public string HowDidYouHearAboutUs { get; set; }
        public string InvoiceRange { get; set; }
        public int BusinessSpan { get; set; } = 0;
        public string EstimatedAnnualRevenue { get; set; }
        public string MOI { get; set; }
        public string COI { get; set; }
        public string MOU { get; set; }
        public string POD { get; set; }
        public string PrincipalClient { get; set; }
        public string ReferralName { get; set; }
        public int TotalCredit { get; set; }
        public string EmailAddress { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public bool IsAccountApproved { get; set; }
        public string AccountType { get; set; }
    }
}
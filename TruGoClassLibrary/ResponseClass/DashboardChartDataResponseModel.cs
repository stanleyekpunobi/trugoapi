﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
    public class DashboardChartDataResponseModel
    {
        public List<ChartData> ThisMonthChartData { get; set; }

        public DateTime[] MonthlyLabels { get; set; }

        public List<ChartData> MonthlyChartData { get; set; }
        
        public DashboardChartDataResponseModel()
        {
            MonthlyChartData = new List<ChartData>();
            ThisMonthChartData = new List<ChartData>();
        }
    }

    public class ChartData
    {
        public int DataValue { get; set; }
        public DateTime DateValue { get; set; }
        public string ChartType { get; set; }

        public int Month { get; set; }

        public string Monthname
        {
            get
            {
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
   public class PaymentResponseModel
    {
        public int Id { get; set; }

        public string ProfileId { get; set; }

        public UserprofileData Profile { get; set; }

        public int InvoiceId { get; set; }

        public InvoiceResponseModel Invoice { get; set; }

        public int InvoicePercentage { get; set; }

        public int Amount { get; set; }

        public DateTime Date_Created { get; set; }
    }
}

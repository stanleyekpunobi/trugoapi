﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
    public class SellerDashboardResponseModel
    {
        public int TotalInvoicesSold { get; set; }
        
        public int NumberOfInvoicesSold { get; set; }

        public int PendingInvoices { get; set; }

        public int InvoicesSold { get; set; }

        public int InvoicesInProgress { get; set; }

        public int CreditBalance { get; set; }

        public List<SellerDashboardInvoiceModel> Invoices { get; set; }

        public List<NotificationResponseModel> Notifications { get; set; }

        public SellerDashboardResponseModel()
        {
            Invoices = new List<SellerDashboardInvoiceModel>();
            Notifications = new List<NotificationResponseModel>();
        }
    }

    public class SellerDashboardInvoiceModel
    {
        public string InvoiceTitle { get; set; }
        public DateTime MaturityDate { get; set; }
        public int Amount { get; set; }
        public int SalePrice { get; set; }
        public string InvoiceStatus { get; set; }
        public string InvoiceType { get; set; }
    }

   
}

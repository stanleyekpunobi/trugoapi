﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
    public class NotificationResponseModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Message { get; set; }

        public DateTime Date_Created { get; set; }

        public bool IsViewed { get; set; }
    }
}

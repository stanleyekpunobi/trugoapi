﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
    public class InvestorDashboardResponseModel
    {
        public int TotalInvoices { get; set; }
        public int PendingInvoices { get; set; }

        public int TotalEarnings { get; set; }

        public int InvoicesBought { get; set; }

        public int InvoicesProcessed { get; set; }

        public int CreditBalance { get; set; }

        public List<InvestorDashboardInvoiceModel> Invoices { get; set; }

        public InvestorDashboardResponseModel()
        {
            Invoices = new List<InvestorDashboardInvoiceModel>();
        }
    }

    public class InvestorDashboardInvoiceModel
    {
        public string Title { get; set; }
        public DateTime MaturityDate { get; set; }
        public int Amount { get; set; }
        public int Earning { get; set; }
        public int SalePrice { get; set; }

        public string InvoiceStatus { get; set; }
    }
}

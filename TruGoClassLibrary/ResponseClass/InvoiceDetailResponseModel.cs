﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
   public class InvoiceDetailResponseModel
    {
        public InvoiceResponseModel Invoice { get; set; }
        public List<PaymentResponseModel> Payments { get; set; }

        public InvoiceDetailResponseModel()
        {
            Invoice = new InvoiceResponseModel();
            Payments = new List<PaymentResponseModel>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
    public class WithdrawCreditResponse
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public DateTime Date_Estimate { get; set; }
        public DateTime Date_Created { get { return DateTime.Now; } set { } }
        public UserprofileData Profile { get; set; }
        public bool Verified { get; set; }
        public bool Status { get; set; }
    }
}

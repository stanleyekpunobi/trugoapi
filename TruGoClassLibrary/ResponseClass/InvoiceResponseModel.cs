﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruGoClassLibrary.ResponseClass
{
    public class InvoiceResponseModel
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceTitle { get; set; }
        public string Client { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public int Amount { get; set; }
        public int DiscountRate { get; set; }
        public int SalePrice { get; set; }
        public string Comment { get; set; }
        public string Invitations { get; set; }
        public string ProfileId { get; set; }
        public UserprofileData Profile { get; set; }
        public string InvoicePath { get; set; }
        public DateTime Date_Created { get; set; }
        public string AdminStatus { get; set; }
        public bool Verified { get; set; }
        public string UserStatus { get; set; }
        public bool IsFeatured { get; set; }
        public Guid InvoiceGuid { get; set; }
    }
}
